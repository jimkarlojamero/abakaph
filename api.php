<?php

	require_once(dirname(__FILE__) . '/php/config/config.php');
	require_once(dirname(__FILE__) . '/php/misc/mailer/mailer.class.php');
	// MCRYPT AES Encryption: Warning - This function has been DEPRECATED as of PHP 7.1.0. Relying on this function is highly discouraged.
	// require_once(dirname(__FILE__) . '/php/misc/mcrypt.class.php');
	require_once(dirname(__FILE__) . '/php/misc/openssl.class.php');
	require_once(dirname(__FILE__) . '/php/misc/jwt_helper.php');
	require_once(dirname(__FILE__) . '/php/misc/array.dictionary.php');

	$cipher = NEW cipher();

	if (isset($_POST['controller']) && !in_array($_POST['controller'], $exemption_array)) { $logs->write_logs('RAW - Request', 'api.php', array(array("_POST" => $_POST, "_GET" => $_GET))); }

	if (isset($_POST['controller']) && !in_array($_POST['controller'], $exemption_array)) {
		foreach ($_POST as $key => $value) $_POST[$key] = ($key === 'Authorization') ? $value : $cipher->decrypt($value);
		foreach ($_GET as $key => $value) $_GET[$key] = $cipher->decrypt($value);
	}

	$controller = trim((isset($_GET['controller'])) ? $_GET['controller'] : ((isset($_POST['controller'])) ? $_POST['controller'] : NULL));
	$function_name = trim((isset($_GET['function_name'])) ? $_GET['function_name'] : ((isset($_POST['function_name'])) ? $_POST['function_name'] : NULL));
	$global_response = NULL;

	if (!in_array($controller, $exemption_array) && (isset($_GET['web_uri_string']) && (strpos($_GET['web_uri_string'], 'assets/') === false))) { $logs->write_logs('Sanitized - ' . ((is_null($function_name) || !$function_name) ? 'Access' : $function_name), $controller . '.controller.php', array(array("_POST" => $_POST, "_GET" => $_GET))); }

	if (!is_null($controller)) {
		if (file_exists(dirname(__FILE__) . '/php/controller/' . $controller . '.controller.php')) {
			require_once (dirname(__FILE__) . '/php/controller/' . $controller . '.controller.php');
			$class = new $controller();

			if (is_callable(array($class, $function_name))) {
				try {
					$request_array = array_merge($_GET, $_POST);
					unset($request_array['controller']);
					unset($request_array['function_name']);
					$result = call_user_func(array($class, $function_name), $request_array);
					if (!is_array($result)) { $result = json_decode($result, true); }
					$global_response = json_encode($result, JSON_UNESCAPED_SLASHES);
				} catch (Exception $e) {
					$global_response = json_encode(array("response"=>"Error", "description"=>$e->getMessage()));
				}
			} else {
				$global_response = json_encode(array("response"=>"Error", "description"=>"Function does not exsist."));
			}
		} else {
			$global_response = $error000;
		}
	} else {
		$global_response = $error000;
	}

	if ($controller != 'log' && $controller != 'env' && (isset($_GET['web_uri_string']) && (strpos($_GET['web_uri_string'], 'assets/') === false))) { $logs->write_logs('Response - ' . $function_name, $controller . '.controller.php', array(json_decode($global_response, true))); }
	die($global_response);

?>