<?php

	require_once(dirname(__FILE__) . '/php/config/config.php');
	require_once(dirname(__FILE__) . '/php/misc/array.dictionary.php');

	try {
		$ar_headers = apache_request_headers();
		$temp_headers = "";
		$jwt_headers = array();

		foreach ($ar_headers as $ar_header => $ar_value) { $temp_headers .= "$ar_header: $ar_value\n"; }

		$parsed_headers = array_map(function($x) { return array_map("trim", explode(":", $x, 2)); }, array_filter(array_map("trim", explode("\n", $temp_headers))));

		for ($i=0; $i < count($parsed_headers); $i++) { $jwt_headers[$parsed_headers[$i][0]] = $parsed_headers[$i][1]; }
	} catch (Exception $e) {
		response('error', $e->getMessage());
	}

	foreach ($jwt_headers as $key => $value) if (isset($jwt_headers[$key]) && in_array($key, $header_dictionary)) { $_POST[$key] = $value; }

	if (isset($_GET['web_uri_string']) && !empty($_GET['web_uri_string'])) {
		$_GET['web_uri_string'] = trim(trim($_GET['web_uri_string']), '/');

		if (array_key_exists($_GET['web_uri_string'], $routes_array)) {
			if (isset($routes_array[$_GET['web_uri_string']]['type']) && $routes_array[$_GET['web_uri_string']]['type'] === 'page') {
				
				if (isset($routes_array[$_GET['web_uri_string']]['session']) && $routes_array[$_GET['web_uri_string']]['session'] && WEB_APP_SESSION_STATUS !== 1) {
					header('Location: ' . $base_domain . '/login');
					exit();
				}

				die(include_once($base . '/pages/' . $_GET['web_uri_string'] . '.php'));
			}

			$_POST['controller'] = $routes_array[$_GET['web_uri_string']]['controller'];
			$_POST['function_name'] = $routes_array[$_GET['web_uri_string']]['function_name'];
			unset($_GET['web_uri_string']);
		}

	} else {
		die((file_exists($base . '/pages/index.php')) ? include_once($base . '/pages/index.php'): null);
	}

	if ((count($_GET) > 0) || (count($_POST) > 0)) {
		$protocol = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') ? 'https://' : 'http://';
		$domain = $protocol . $_SERVER['HTTP_HOST'] . str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']);
		$url = $domain . "api.php?" . http_build_query($_GET);
		$cURL = curl_init();

		foreach ($_POST as $key => $value) {
			if (is_array($value)) {
				$_POST[$key] = json_encode($value);
			}
		}
		
		curl_setopt($cURL, CURLOPT_URL, $url);
		curl_setopt($cURL, CURLOPT_POST, 1);
		curl_setopt($cURL, CURLOPT_POSTFIELDS, $_POST);
		curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($cURL, CURLOPT_SSL_VERIFYHOST, false); 
		
		try {
			$server_output = curl_exec($cURL);
		} catch (Exception $e) {
			response('error', $e->getMessage());
		}
		
		curl_close ($cURL);
		flush();

		die($server_output);
	} else {
		response('error', "Invalid Access.");
	}

?>