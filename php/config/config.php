<?php

	ob_start();
	// header('Location: api.php');
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	// error_reporting(E_ERROR | E_PARSE); // Remove WARNING (Temporary Solution)
	/*header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	header("Access-Control-Allow-Headers: X-Requested-With");
	header('X-Frame-Options: DENY');
	header('Content-Type: application/json');*/
	header('Cache-Control: no-cache');
	set_time_limit(0);
	ini_set('memory_limit', '-1');
	ini_set('mysql.connect_timeout','0');
	ini_set('max_execution_time', '0');
	ini_set('date.timezone', 'Asia/Manila');

	define('LOGS_STATUS', 1);
	define('ENV_STATUS', 1);
	define('DATE_NOW', DATE('Y-m-d H:i:s'));
	// define('APP_NAME', explode('/', $_SERVER['PHP_SELF'])[1]);

	// Handles session for the whole API/WEB APP
	session_manager();

	$protocol = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') ? 'https://' : 'http://';
	$current_domain = $protocol . $_SERVER['HTTP_HOST'] . str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']);
	$base_domain = ($_SERVER['HTTP_HOST'] == 'localhost') ? $protocol . $_SERVER['HTTP_HOST'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $protocol . $_SERVER['HTTP_HOST'];
	$base = (($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']);
	$file_name = substr(strtolower(basename($_SERVER['PHP_SELF'])), 0, strlen(basename($_SERVER['PHP_SELF'])));
	$error000 = json_encode(array("response"=>"error", "description"=>"Invalid API Access."));
	$error0003 = json_encode(array("response"=>"error", "error_code"=>'0003', "description"=>"Unrecognized Token."));
	$allowed_domains = [
		'http://localhost/abakaph',
		'http://abaka.space',
		'http://abaka.ph',
		'https://localhost/abakaph',
		'https://abakaph.space',
		'https://abaka.ph',
	];

	if (in_array($base_domain, $allowed_domains)) {
		header('Access-Control-Allow-Origin: ' . $base_domain, true);
	}

	require_once ($base . '/php/misc/array.dictionary.php');
	require_once ($base . '/php/misc/logs.class.php');
	require_once ($base . '/php/controller/env.controller.php');

	$logs = NEW logs;
	$environment = NEW env;

	if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
		redirect($base_domain, true, 'Warning - Invalid Access', $file_name, array(array("_POST" => $_POST, "_GET" => $_GET)));
	}

	$env = (object) $environment->parse_env();

	if (!file_exists($base . '/.htaccess')) { generate_htaccess($env->domain_name); }

	function process_qry ($query, array $args = array()) {
		$message = NULL;
		$output = array();
		global $env;
		global $logs;
		global $file_name;

		try {
			$pdo = new PDO (
				'mysql:host=' . $env->host . ';port=' . $env->port . ';dbname=' . $env->db,
				$env->un,
				$env->pw,
				array(
					PDO::ATTR_PERSISTENT => true,
					PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		} catch (PDOException $e) {
			$pdo = NULL;
			response('error', str_replace("\r\n", "", "Failed to connect to MySQL: " . $e->getMessage() . " Error Code: (" . $e->getCode() . ")."), NULL, true, 'Response - MySQL', $file_name);
		}

		try {
			$pdo_statement = $pdo->prepare($query);
		} catch (PDOException $e) {
			$pdo = NULL;
			response('error', str_replace("\r\n", "", "MySQLi Prepare Failed: " . $e->getMessage() . " Error Code: (" . $e->getCode() . ")."), NULL, true, 'Response - MySQL', $file_name);
		}

		if (count($args) > 0) {
			for ($i=0; $i < count($args); $i++) {
				$type = PDO::PARAM_STR;

				if (is_int($args[$i])) {
					$type = PDO::PARAM_INT;
					$args[$i] = filter_var(trim(urlencode($args[$i])), FILTER_SANITIZE_NUMBER_INT);
				} elseif (is_bool($args[$i])) {
					$type = PDO::PARAM_BOOL;
					$args[$i] = filter_var(trim(urlencode($args[$i])), FILTER_SANITIZE_NUMBER_INT);
				} elseif (is_null($args[$i])) {
					$type = PDO::PARAM_NULL;
					$args[$i] = NULL;
				} else {
					$args[$i] = filter_var(trim(urldecode($args[$i])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
				}

				try {
					$pdo_statement->bindValue($i+1, $args[$i], $type);
				} catch (Exception $e) {
					$pdo = NULL;
					response('error', str_replace("\r\n", "", "MySQLi Bind Parameter Failed: " . $e->getMessage() . " Error Code: (" . $e->getCode() . ")."), NULL, true, 'Response - MySQL', $file_name);
				}
			}
		}

		try {
			$pdo_statement->execute();

			if ($pdo_statement->rowCount()) {
				$output = (preg_match('/\b(update|insert|delete)\b/', strtolower($query)) === 1) ? array(array('response'=>'Success')) : $pdo_statement->fetchAll(PDO::FETCH_ASSOC);
			}
		} catch (PDOException $e) {
			$pdo = NULL;
			response('error', str_replace("\r\n", "", "MySQLi Execution Failed: " . $e->getMessage() . " Error Code: (" . $e->getCode() . ")."), NULL, true, 'Response - MySQL', $file_name);
		}

		$pdo = NULL;

		if (count($output) > 0) {
			for ($ii=0; $ii < count($output); $ii++) {
				foreach ($output[$ii] as $key => $value) {
					$output[$ii][$key] = html_entity_decode($value, ENT_QUOTES, 'UTF-8');
					$temp_val = $output[$ii][$key];
					$temp_val = str_replace("\r\n", "<br/>", $temp_val);
					$temp_val = str_replace("\r", "<br/>", $temp_val);
					$temp_val = str_replace("\n", "<br/>", $temp_val);
					// $temp_val = str_replace('"', "&#34;", $temp_val);
					// $temp_val = str_replace("'", "&#39;", $temp_val);
					// $temp_val = str_replace('/', "&#47;", $temp_val);
					// $temp_val = str_replace("\\", "&#92;", $temp_val);
					$output[$ii][$key] = $temp_val;
				}
			}
		}

		// ob_flush();
		flush();

		return ['response' => 'Success', 'data' => $output];
		// response('success', NULL, $output);
	}

	function process_params($default, $required, $params) {
		$keys = array_keys($default);
		$data = array_merge($default, $params);
		$array_diff = array_values(array_diff($required, array_keys($params)));
		$env = NULL;

		if (count($array_diff) > 0) {
			response('error', "Parameter ( " . $array_diff[0] . " ) is required.");
		} else {
			foreach ($params as $key => $value) {
				if ((in_array($key, $required) && $params[$key] == NULL)) {
					response('error', "Empty value on Parameter ( " . $key . " ).");
				} elseif (!in_array($key, $keys)) {
					response('error', "Invalid value on Parameter ( " . $key . " ).");
				}
			}
		}

		return $data;
	}

	function randomizer($len, $norepeat = true) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$max = strlen($chars) - 1;

		if ($norepeat && $len > $max + 1) {
			throw new Exception("Non repetitive random string can't be longer than charset");
		}

		$rand_chars = array();

		while ($len) {
			$picked = $chars[mt_rand(0, $max)];

			if ($norepeat) {
				if (!array_key_exists($picked, $rand_chars)) {
					$rand_chars[$picked] = true;
					$len--;
				}
			} else {
				$rand_chars[] = $picked;
				$len--;
			}
		}

		return implode('', $norepeat ? array_keys($rand_chars) : $rand_chars);
	}

	function num_randomizer($len, $norepeat = true) {
		$chars = "0123456789";
		$max = strlen($chars) - 1;

		if ($norepeat && $len > $max + 1) {
			throw new Exception("Non repetitive random string can't be longer than charset");
		}

		$rand_chars = array();

		while ($len) {
			$picked = $chars[mt_rand(0, $max)];

			if ($norepeat) {
				if (!array_key_exists($picked, $rand_chars)) {
					$rand_chars[$picked] = true;
					$len--;
				}
			} else {
				$rand_chars[] = $picked;
				$len--;
			}
		}

		return implode('', $norepeat ? array_keys($rand_chars) : $rand_chars);
	}

	function validate_email($email) {
		$isValid = true;
		$atIndex = strrpos($email, "@");

		if (is_bool($atIndex) && !$atIndex) {
			$isValid = false;
		} else {
			$domain = substr($email, $atIndex+1);
			$local = substr($email, 0, $atIndex);
			$localLen = strlen($local);
			$domainLen = strlen($domain);
			$domain_arr = explode(".", $domain);

			if ($localLen < 1 || $localLen > 64) {
				$isValid = false;
			} else if ($domainLen < 1 || $domainLen > 255) {
				$isValid = false;
			} else if ($local[0] == '.' || $local[$localLen-1] == '.') {
				$isValid = false;
			} else if (preg_match('/\\.\\./', $local)) {
				$isValid = false;
			} else if (!preg_match('/^[A-z0-9\\+-\\.]+$/', $domain)) {
				$isValid = false;
			} else if (preg_match('/\\.\\./', $domain)) {
				$isValid = false;
			} else if(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local))) {
				if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local))) {
					$isValid = false;
				}
			} else if (strrpos($domain, ".") === false) {
				$isValid = false;
			} else if (end($domain_arr) == NULL) {
				$isValid = false;
			}
		}

		return $isValid;
	}

	function dd($params) {
		if (is_array($params)) {
			die(json_encode($params, JSON_PRETTY_PRINT));
		}
		
		die(var_dump($params));
	}

	function response($type, $message = NULL, $data = NULL, $log = NULL, $file = NULL, $source = NULL, $terminate = 1) {
		$output = array('response' => strtolower($type));
		if (!is_null($message)) { $output['message'] = trim($message); }
		if (!is_null($data)) { $output['data'] = $data; }
		if (!is_null($message) && $log === true && !is_null($file) && !is_null($source)) {
			global $logs;
			$logs->write_logs($file, $source, $output);
		}

		if ($terminate === 1) {
			if (ob_get_contents()) ob_end_flush();
			// ob_end_clean();
			die(json_encode($output));
		} else {
			return json_encode($output);
		}
	}

	function redirect($url, $log = NULL, $file = NULL, $source = NULL, $data = NULL) {
		if (!is_null($data) && $log === true && !is_null($file) && !is_null($source)) {
			global $logs;
			$logs->write_logs($file, $source, $output);
		}
		
		if (ob_get_contents()) ob_end_flush();
		// if (ob_get_contents()) ob_end_clean();
		header('Location: ' . $url);
	}

	function generate_htaccess($host = null) {
		global $base;
		$htaccess = file_get_contents($base . '/php/misc/.htaccess.copy');
		$htaccess = ($host) ? str_replace('localhost', $host, $htaccess) : $htaccess;
		$fh = fopen($base . '/.htaccess', "w");
		fwrite($fh, $htaccess);
		fclose($fh);
	}

	function session_manager() {
		if (!isset($_SESSION)) { session_start(); }
		if (isset($_SESSION['web_app_session_id']) && isset($_SESSION['web_app_session_un']) && isset($_SESSION['web_app_session_pass_key'])) {
			if (base64_encode(base64_encode($_SESSION["web_app_session_id"]) . '::' . base64_encode($_SESSION["web_app_session_un"])) != $_SESSION['web_app_session_pass_key']) {
				define('WEB_APP_SESSION_STATUS', 0);
				unset($_SESSION["web_app_session_id"]);
				unset($_SESSION["web_app_session_un"]);
				unset($_SESSION["web_app_session_pass_key"]);
			} else {
				define('WEB_APP_SESSION_STATUS', 1);
			}
		} else {
			if (!defined('WEB_APP_SESSION_STATUS')) {
				define('WEB_APP_SESSION_STATUS', 0);
			}
		}
	}

	function user() {
		if (!isset($_SESSION)) { session_start(); }
		if (!isset($_SESSION['web_app_session_pass_key']) || !$_SESSION['web_app_session_pass_key']) return [];
		
		$step_1 = explode('::', base64_decode($_SESSION['web_app_session_pass_key']));

		if (!isset(explode('::', base64_decode($_SESSION['web_app_session_pass_key']))[1])) return [];

		$step_2 = base64_decode(base64_decode(explode('::', base64_decode($_SESSION['web_app_session_pass_key']))[1]));
		$output = process_qry("SELECT * FROM `users` WHERE `email` = ? AND `status` = 1 LIMIT 1", [$step_2]);

		if (isset($output['data'][0]['password'])) unset($output['data'][0]['password']);

		return (object)$output['data'][0];
	}

?>
