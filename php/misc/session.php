<?php

	require_once((($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']) . '/php/config/config.php');

	if (!isset($_SESSION)) { session_start(); }

	if (isset($_GET['web_app_session_id']) && isset($_GET['web_app_session_un']) && isset($_GET['web_app_session_pass_key'])) {
		if (base64_encode(base64_encode($_GET["web_app_session_id"]) . '::' . base64_encode($_GET["web_app_session_un"])) != $_GET['web_app_session_pass_key']) {
			define('WEB_APP_SESSION_STATUS', 0);
			unset($_SESSION["web_app_session_id"]);
			unset($_SESSION["web_app_session_un"]);
			unset($_SESSION["web_app_session_pass_key"]);
			header('Location: ' . $base_domain);
			exit();
		} else {
			define('WEB_APP_SESSION_STATUS', 1);
			$_SESSION['web_app_session_id'] = $_GET['web_app_session_id'];
			$_SESSION['web_app_session_un'] = $_GET['web_app_session_un'];
			$_SESSION['web_app_session_pass_key'] = $_GET['web_app_session_pass_key'];
			session_write_close();
			header('Location: ' . $base_domain . ((isset($_GET['destination'])) ? '/' . $_GET['destination'] : null));
			exit();
		}
	} else {
		unset($_SESSION["web_app_session_id"]);
		unset($_SESSION["web_app_session_un"]);
		unset($_SESSION["web_app_session_pass_key"]);
		header('Location: ' . $base_domain);
		exit();
	}

?>