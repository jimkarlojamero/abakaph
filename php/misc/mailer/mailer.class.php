<?php

	class mailer {

		public function send_mail($recipient, $recipient_name, $subject, $message) {
			require_once(dirname(__FILE__) . '/PHPMailerAutoload.php');
			global $env;

			if (!isset($env->mailer_account_username) || !isset($env->mailer_account_password)) {
				return array("response" => "Error", "data" => array(array("description" => "SMTP is not yet configured.")));
			}

			//Create a new PHPMailer instance
			$mail = new PHPMailer;
			//Tell PHPMailer to use SMTP
			$mail->isSMTP();
			//Enable SMTP debugging
			// 0 = off (for production use)
			// 1 = client messages
			// 2 = client and server messages
			$mail->SMTPDebug = 0;
			//Ask for HTML-friendly debug output
			$mail->Debugoutput = 'html';
			//Set the hostname of the mail server
			$mail->Host = $env->mailer_smtp;
			//Enable SMTP Secure
			// none = Unsecured
			// ssl = Secured
			$mail->SMTPSecure = $env->mailer_secure;
			//Set the SMTP port number - likely to be 25, 465 or 587
			$mail->Port = $env->mailer_port;
			//Whether to use SMTP authentication
			$mail->SMTPAuth = true;
			//Username to use for SMTP authentication
			$mail->Username = $env->mailer_account_username;
			//Password to use for SMTP authentication
			$mail->Password = $env->mailer_account_password;
			//Set who the message is to be sent from
			$mail->setFrom($env->mailer_account_username, $env->mailer_from_name);
			//Set an alternative reply-to address
			$mail->addReplyTo($env->mailer_account_username, $env->mailer_from_name);
			//Set who the message is to be sent to
			$mail->addAddress($recipient, $recipient_name);
			//Set the subject line
			$mail->Subject = $subject;
			//Read an HTML message body from an external file, convert referenced images to embedded,
			//convert HTML into a basic plain-text alternative body
			$mail->msgHTML($message, dirname(__FILE__));
			//Replace the plain text body with one created manually
			$mail->AltBody = $message;
			//Attach an image file
			// $mail->addAttachment('images/phpmailer_mini.png');

			//Custom Header
			$mail->addCustomHeader("MIME-Version", "1.0");
			$mail->addCustomHeader("Organization" , $recipient_name); 
			$mail->addCustomHeader("Content-Transfer-encoding" , "8bit");
			$mail->addCustomHeader("Message-ID" , "<".md5(uniqid(time()))."@{$_SERVER['SERVER_NAME']}>");
			$mail->addCustomHeader("X-MSmail-Priority" , "High");
			$mail->addCustomHeader("X-Mailer" , "PHPMailer 5.1 (phpmailer.sourceforge.net)");
			$mail->addCustomHeader("X-MimeOLE" , "5.1 (phpmailer.sourceforge.net)");
			$mail->addCustomHeader("X-Sender" , $env->mailer_account_username);
			$mail->addCustomHeader("X-AntiAbuse" , "This is a solicited email for - ".$recipient_name." mailing list.");
			$mail->addCustomHeader("X-AntiAbuse" , "Servername - {$_SERVER['SERVER_NAME']}");
			$mail->addCustomHeader("X-AntiAbuse" , $env->mailer_account_username);

			//send the message, check for errors
			if (!$mail->send()) {
				return array("response" => "Error", "data" => array(array("description" => $mail->ErrorInfo)));
			} else {
				return array("response" => "Success");
			}
		}

	}

?>