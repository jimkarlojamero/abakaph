<?php

	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');

	if (!isset($_SESSION)) { session_start(); }
	if (isset($_SESSION['env_session_id']) && isset($_SESSION['env_session_un']) && isset($_SESSION['env_session_pass_key'])) {
		if (base64_encode(base64_encode($_SESSION["env_session_id"]) . '::' . base64_encode($_SESSION["env_session_un"])) != $_SESSION['env_session_pass_key']) {
			define('SESSION_STATUS', 0);
			unset($_SESSION["env_session_id"]);
			unset($_SESSION["env_session_un"]);
			unset($_SESSION["env_session_pass_key"]);
		} else { define('SESSION_STATUS', 1); }
	} else { define('SESSION_STATUS', 0); }

	echo "data: " . SESSION_STATUS . "\n\n";
	flush();

?>