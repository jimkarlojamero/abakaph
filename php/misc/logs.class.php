<?php

	require_once((($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']) . '/php/config/config.php');

	if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
		redirect($base_domain, true, 'Warning - Invalid Access', $base, array(array("_POST" => $_POST, "_GET" => $_GET)));
	}

	class logs {
		private $storage;

		public function __construct() {
			global $base;
			$this->storage = $base . '/php/tools/logs/archive/';
		}

		public function write_logs($function, $file_name, $message) {
			if (LOGS_STATUS !== 1) { return; }

			$path = $this->storage . DATE('Y-m-d');
			$file = $path . "/" . $function . ".txt";

			if (!is_dir($path)) { mkdir($path, 0777, true); }
			if (!file_exists($file)) { fopen($file, "w"); }
			
			$fullmsg = json_encode(array("id"=>DATE('Y-md-Hi-') . strtoupper(randomizer(4)), "timestamp"=>DATE("M d, Y H:i:s (D)"), "client"=>$this->get_client_ip(), "file" => $file_name, "data"=>$message)) . "\r\n";
			$fhandler = fopen($file, "a+");

			fwrite($fhandler, $fullmsg);
			fclose($fhandler);
			return;
		}

		public function get_client_ip() {
			$ipAddresses = array();
			
			if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
				$ipAddresses['proxy'] = isset($_SERVER["HTTP_CLIENT_IP"]) ? $_SERVER["HTTP_CLIENT_IP"] : $_SERVER["REMOTE_ADDR"];
				$ipAddresses['user'] = $_SERVER["HTTP_X_FORWARDED_FOR"];
			} else {
				$ipAddresses['user'] = isset($_SERVER["HTTP_CLIENT_IP"]) ? $_SERVER["HTTP_CLIENT_IP"] : $_SERVER["REMOTE_ADDR"];
			}

			return $ipAddresses['user'];
		}

	}

?>