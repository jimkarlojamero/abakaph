<?php

	if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
		require_once (dirname(__FILE__) . '/logs.class.php');
		$logs = NEW logs;
		$logs->write_logs('Warning - Invalid Access', substr(strtolower(basename($_SERVER['PHP_SELF'])),0,strlen(basename($_SERVER['PHP_SELF']))), array(array("_POST" => $_POST, "_GET" => $_GET)));
		header('Location: ../../');
	}

	class cipher {
		private $_cipher = 'AES-128-CBC';
		private $_option = OPENSSL_RAW_DATA;
		private $_key;
		private $_iv;

		public function __construct() {
			global $env;
			$this->_key = $env->app_token;
			$this->_iv = $env->app_iv;
		}

		public function encrypt($text) {
			return base64_encode(openssl_encrypt($text, $this->_cipher, $this->_key, $this->_option, $this->_iv));
		}

		public function decrypt($text) {
			return openssl_decrypt(base64_decode($text), $this->_cipher, $this->_key, $this->_option, $this->_iv);
		}

	}

?>