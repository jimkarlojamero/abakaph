<!--<div class="range-label">Displaying {{ range.lower }} - {{ range.upper }} of {{ range.total }}</div>

<div ng-if="1 < pages.length">
	<button type="button" class="btn btn-dark" ng-class="{ disabled : pagination.current == 1 }" ng-click="setCurrent(pagination.current - 1)">&lsaquo;</button>
	<button type="button" class="btn btn-dark" ng-class="{ disabled : pagination.current == pagination.last }" ng-click="setCurrent(pagination.current + 1)">&rsaquo;</button>
</div>-->
<br/><div class="range-label">Displaying {{ range.lower }} - {{ range.upper }} of {{ range.total }}</div><br/>

<nav aria-label="Page navigation" ng-if="1 < pages.length">
    <ul class="pagination justify-content-center" ng-if="1 < pages.length || !autoHide" >
        <li class="page-item" ng-if="boundaryLinks" ng-class="{ disabled : pagination.current == 1 }">
            <a class="page-link" href="#" title="Previous" ng-class="{ disabled : pagination.current == 1 }" ng-click="setCurrent(pagination.current - 1)">Previous</a>
        </li>
        <li ng-repeat="pageNumber in pages track by $index" class="page-item" ng-class="{ active : pagination.current == pageNumber, disabled : pageNumber == '...' }">
            <a class="page-link" ng-click="setCurrent(pageNumber)" hre="#">{{ pageNumber }}</a>
        </li>
        <li class="page-item" ng-if="boundaryLinks"  ng-class="{ disabled : pagination.current == pagination.last }">
            <a class="page-link" ng-click="setCurrent(pagination.last)" href="#">Next</a>
        </li>
    </ul>
</nav>