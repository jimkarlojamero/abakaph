<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	error_reporting(E_ERROR | E_PARSE); // Remove WARNING (Temporary Solution)
	header('Content-Type: application/json');
	header('Cache-Control: no-cache');
	set_time_limit(0);
	ini_set('memory_limit', '-1');
	// ini_set('mysql.connect_timeout','0');
	ini_set('max_execution_time', '0');
	ini_set('date.timezone', 'Asia/Manila');


	// $error000 = json_encode(array(array("response"=>"Error", "description"=>"Invalid API Access.")));


	 if (!isset($_FILES) || (!$_FILES)) { die($error000); }

	$path = realpath(dirname(__FILE__)) . "../../../assets/dump/sub_images";

    $file_name = substr(strtolower(basename($_SERVER['PHP_SELF'])),0,strlen(basename($_SERVER['PHP_SELF'])));

	if (!file_exists($path)) { mkdir($path, 0777, true); }

	$output = array();

	foreach ($_FILES as $key => $value) {

		// $width = 600;
		// $height = 400;
		$uniqID = uniqid();
		$valid_formats = array('jpg', 'jpeg', 'gif', 'png', 'bmp');

		$name = $_FILES[$key]["name"];
		$size = $_FILES[$key]['size'];
		$tmp  = $_FILES[$key]['tmp_name'];
		$type = $_FILES[$key]['type'];

		$max_file_size = 2;

		if ($name) {
			$ext_arr = explode(".", $name);
			$ext = end($ext_arr);

			if (in_array(strtolower($ext), $valid_formats)) {
				if ($size < ((int) $max_file_size * 100000)) { 
					if (move_uploaded_file($tmp, $path . '/' . $uniqID . '.' . $ext)) {

						array_push($output, array("response"=> "success", "image" => $uniqID . '.' . $ext, "file" => $ext));

					 } else {
					 	array_push($output, array("response" => "Error", "description" => "Unable to upload file. Please try again later."));
					}
				} else {
					array_push($output, array("response" => "Error", "description" => "File exceeds minimum size requirements. Please try again later."));
				}
			} else {
				array_push($output, array("response" => "Error", "description" => "Invalid file format."));
			}
		} else {
			array_push($output, array("response" => "Error", "description" => "Please select a file."));
	    }
	}

	die(json_encode($output));
?>