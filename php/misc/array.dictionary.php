<?php

	static $header_dictionary = [
		'user_id',
		'device_id',
		'fcm_id',
		'platform',
		'Authorization',
		'jwt_ssk',
		'controller',
		'function_name',
		'manufacturer',
		'model',
		'username',
		'password',
		'date',
		'sort',
		'order_by',
		'uid',
	];

	static $routes_array = [
		'test-form' => [
			'type' => 'page'
		],
		'registrants' => [
			'type' => 'page'
		],
		'front-registrants' => [
			'type' => 'page'
		],
		'fcm/send' => [
			'controller' => 'fcm',
			'function_name' => 'send'
		],
		'fcm/single' => [
			'controller' => 'fcm',
			'function_name' => 'single'
		],
		'tools/env/login' => [
			'controller' => 'env',
			'function_name' => 'login'
		],
		'tools/logs/aes_decrypt' => [
			'controller' => 'logs',
			'function_name' => 'aes_decrypt'
		],
		'tools/logs/jwt_decode' => [
			'controller' => 'logs',
			'function_name' => 'jwt_decode'
		],
		'tools/log/get_file_data' => [
			'controller' => 'log',
			'function_name' => 'get_file_data'
		],
		'tools/log/get_folder_data' => [
			'controller' => 'log',
			'function_name' => 'get_folder_data'
		],
		'tools/log/date_folder' => [
			'controller' => 'log',
			'function_name' => 'date_folder'
		],
		'tools/env/send_push' => [
			'controller' => 'env',
			'function_name' => 'send_push'
		],
		'tools/env/generate_env' => [
			'controller' => 'env',
			'function_name' => 'generate_env'
		],
		'municipality' => [
			'controller' => 'abaka',
			'function_name' => 'get_municipality'
		],
		'civil/status' => [
			'controller' => 'abaka',
			'function_name' => 'get_civil_status'
		],
		'register' => [
			'controller' => 'abaka',
			'function_name' => 'register'
		],
		'fetch/registrants' => [
			'controller' => 'abaka',
			'function_name' => 'get_registrants'
		],
		'fetch/delete_registrant' => [
			'controller' => 'abaka',
			'function_name' => 'delete_registrant'
		],
		'fetch/search_query' => [
			'controller' => 'abaka',
			'function_name' => 'search_query'	
		],
		'fetch/municipality_filter' => [
			'controller' => 'abaka',
			'function_name' => 'municipality_filter'	
		],
		'fetch/itemsperpage' => [
			'controller' => 'abaka',
			'function_name' => 'itemsperpage'	
		],
		'migrate/members' => [
			'controller' => 'abaka',
			'function_name' => 'member_migrations'
		],
		'update/members' => [
			'controller' => 'abaka',
			'function_name' => 'update_members'
		],
		'abaka/login' => [
			'controller' => 'abaka',
			'function_name' => 'login'
		],
		'abaka/generate' => [
			'controller' => 'abaka',
			'function_name' => 'export_data'
		],
		'count/members' => [
			'controller' => 'abaka',
			'function_name' => 'get_member_count'
		],
		'front/fget_registrants' => [
			'controller' => 'abaka',
			'function_name' => 'fget_registrants'
		],
	];

	$env_config = [
		"host" => [
			'title' => 'Host',
			'placeholder' => 'Please enter a host',
			'default' => 'localhost',
			'required' => true,
		],
		"un" => [
			'title' => 'Database Username',
			'placeholder' => 'Please enter your database username',
			'required' => true,
		],
		"pw" => [
			'title' => 'Database Password',
			'placeholder' => 'Please enter your database password',
			'type' => 'password',
		],
		"db" => [
			'title' => 'Database Name',
			'placeholder' => 'Please enter your database name',
			'required' => true,
		],
		"port" => [
			'title' => 'Database Port',
			'placeholder' => 'Please enter your database port',
			'default' => '3306',
			'required' => true,
		],
		"jwt_ssk" => [
			'title' => 'JWT SSK',
			'placeholder' => 'Please generate a JWT SSK',
			'default' => randomizer(16),
			'plugin' => 'randomizer',
			'length' => 16,
		],
		"app_token" => [
			'title' => 'App Key',
			'placeholder' => 'Please generate an App Key',
			'default' => randomizer(16),
			'plugin' => 'randomizer',
			'length' => 16,
		],
		"app_iv" => [
			'title' => 'Initialization Vector',
			'placeholder' => 'Please generate an Initialization Vector',
			'default' => randomizer(16),
			'plugin' => 'randomizer',
			'length' => 16,
		],
		"fcm" => [
			'title' => 'FCM',
			'placeholder' => 'Please enter your FCM Server Key',
		],
		"app_name" => [
			'title' => 'App Name',
			'placeholder' => 'Please enter your App Name',
		],
		"domain_name" => [
			'title' => 'Domain Name',
			'placeholder' => 'Please enter your domain name',
			'default' => 'localhost',
		],
		"adm_un" => [
			'title' => 'Admin Username',
			'placeholder' => 'Please enter your Admin Username',
			'default' => 'dev',
			'required' => true,
		],
		"adm_pw" => [
			'title' => 'Admin Password',
			'placeholder' => 'Please enter your Admin Password',
			'default' => 'd3v3l0pm3n+',
			'required' => true,
			'type' => 'password',
		],
		"mailer_sender" => [
			'title' => 'Sender E-Mail Address',
			'placeholder' => 'Please enter your Sender E-Mail Address',
		],
		"mailer_from_name" => [
			'title' => 'From Name',
			'placeholder' => 'Please enter from name',
		],
		"mailer_smtp" => [
			'title' => 'SMTP Server',
			'placeholder' => 'Please enter SMTP server',
		],
		"mailer_account_username" => [
			'title' => 'SMTP Account Username',
			'placeholder' => 'Please enter SMTP Account Username',
		],
		"mailer_account_password" => [
			'title' => 'SMTP Account Password',
			'placeholder' => 'Please enter SMTP Account Password',
			'type' => 'password',
		],
		"mailer_port" => [
			'title' => 'SMTP Port',
			'placeholder' => 'Please enter SMTP Port',
		],
		"mailer_secure" => [
			'title' => 'SMTP Security',
			'placeholder' => 'Please enter SMTP Security',
		],
		"facebook_app_id" => [
			'title' => 'Facebook App ID',
			'placeholder' => 'Please enter Facebook App ID',
		],
	];

	$exemption_array = explode(',', str_replace('.controller.php', '', implode(',',array_values(array_diff_key(scandir($base . '/php/controller'), ['.', '..'])))));

?>