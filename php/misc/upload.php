<?php

	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	error_reporting(E_ERROR | E_PARSE); // Remove WARNING (Temporary Solution)
	header('Content-Type: application/json');
	header('Cache-Control: no-cache');
	set_time_limit(0);
	ini_set('memory_limit', '-1');
	// ini_set('mysql.connect_timeout','0');
	ini_set('max_execution_time', '0');
	ini_set('date.timezone', 'Asia/Manila');


	$error000 = json_encode(array(array("response"=>"Error", "description"=>"Invalid API Access.")));


	/*if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
		require_once (dirname(__FILE__) . '/logs.class.php');
		$logs = NEW logs;
		$logs->write_logs('Warning - Invalid Access', $file_name, array(array("_POST" => $_POST, "_GET" => $_GET)));
		header('Location: ../../api.php');
	}*/

	if (!isset($_FILES) || (!$_FILES)) { die($error000); }

	$path = realpath(dirname(__FILE__)) . "../../../assets/dump";
	$file_name = substr(strtolower(basename($_SERVER['PHP_SELF'])),0,strlen(basename($_SERVER['PHP_SELF'])));

	if (!file_exists($path)) { mkdir($path, 0777, true); }

	$width = 600;
	$height = 400;
	$output = array();
	$uniqID = uniqid();
	$valid_formats = array('jpg', 'jpeg', 'gif', 'png', 'bmp');
	$name = $_FILES["file"]["name"];
	$size = $_FILES['file']['size'];
	$tmp  = $_FILES['file']['tmp_name'];
	$type = $_FILES['file']['type'];
	$max_file_size = 2;

	if ($name) {
		$ext_arr = explode(".", $name);
		$ext = end($ext_arr);

		if (in_array(strtolower($ext), $valid_formats)){
			if ($size < ((int) $max_file_size * 100000)) {
				if (move_uploaded_file($tmp, $path . '/' . $uniqID . '.' . $ext)) {
					if (in_array(strtolower($ext), array('png', 'jpg', 'jpeg'))) {
						list($width_orig, $height_orig) = getimagesize($path . '/' . $uniqID . '.' . $ext);
						$ratio_orig = $width_orig/$height_orig;

						//Height Driven
						if (($width / $height )< $ratio_orig) {
							$width = ($height * $ratio_orig);
						} else {
							$height = ($width / $ratio_orig);
						}

						if (in_array(strtolower($ext), array('jpg', 'jpeg'))) {
							$imgSrc = $path . '/' . $uniqID . '.' . $ext;
							$image = imagecreatefromjpeg($imgSrc);

						} else if (strtolower($ext) == 'png') {
							$imgSrc = $path . '/' . $uniqID . '.' . $ext;
							$image = imagecreatefrompng($imgSrc);
						}

						$image_p = imagecreatetruecolor($width, $height);
						$almostblack = imagecolorallocate($image_p,255,255,255);
						imagefill($image_p, 0, 0, $almostblack);
						$black = imagecolorallocate($image_p, 0, 0 ,0);
						imagecolortransparent($image_p, $almostblack);
						imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
						imagejpeg($image_p, $path . '/' . $uniqID . '.' . $ext, 80);
					}

					array_push($output, array("response" => "Success", "data"=>array(array("description"=> strtoupper($ext) . " file has been successfully uploaded.", "image" => $uniqID . '.' . $ext, "file" => $ext))));
				} else {
					array_push($output, array("response" => "Error", "description" => "Unable to upload file. Please try again later."));
				}
			} else {
				array_push($output, array("response" => "Error", "description" => "File exceeds minimum size requirements. Please try again later."));
			}
		} else {
			array_push($output, array("response" => "Error", "description" => "Invalid file format."));
		}
	} else {
		array_push($output, array("response" => "Error", "description" => "Please select a file."));
	}

	ob_flush();
	flush();

	// $logs->write_logs($output[0]['response'] . ' - File Upload', $file_name, array(array("_POST" => $_POST, "_FILES" => $_FILES, "data" => $output)));
	die(json_encode($output));


?>