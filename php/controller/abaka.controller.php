<?php

	require_once((($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']) . '/php/config/config.php');

	if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
		redirect($base_domain, true, 'Warning - Invalid Access', $base, array(array("_POST" => $_POST, "_GET" => $_GET)));
	}

	class abaka extends mailer {

		function __constructor() {

		}

		public function get_civil_status() {
			response('success', null, process_qry("SELECT `id`, `name` FROM `civil_status` WHERE `status` = 1", [])['data']);
		}

		public function get_municipality() {
			response('success', null, process_qry("SELECT `id`, `name` FROM `municipality` WHERE `status` = 1", [])['data']);
		}

		public function get_registrants($param) {
			$param_val = [
				(int) (($param['pagenum']) - 1) * (int) $param['itemperpage'],
				(int) $param['itemperpage'],
			];

			$registrants = process_qry("SELECT `members`.`id`, `members`.`first_name`,`members`.`height`, `members`.`weight`, `members`.`spouse_name`, `members`.`num_children` ,`members`.`middle_name`, `members`.`last_name`, `members`.`birthday`, `members`.`birthplace`, `members`.`gender`, `members`.`contact`, `members`.`municipality`, `members`.`address`, `members`.`occupation`, `members`.`religion`, `members`.`civil_status`, `members`.`email`, `members`.`status`, `members`.`verified`, `members`.`agreement`, `members`.`misc`, `municipality`.`name` AS `municipality_name` FROM `members` INNER JOIN `municipality` ON `municipality`.`id` = `members`.`municipality` WHERE `members`.`status` = 1 AND `members`.`deleted_at` IS NULL ORDER BY `members`.`id` DESC LIMIT " . $param_val[0] . ", " . $param_val[1], []);

			if (isset($registrants['response']) && strtolower($registrants['response']) == 'success' && count($registrants['data']) > 0) {

				//$registrants = process_qry("SELECT COUNT(`members`.`id`) AS `total_count` FROM `members` INNER JOIN `municipality` ON `municipality`.`id` = `members`.`municipality` WHERE `members`.`status` = 1 AND `members`.`deleted_at` IS NULL ORDER BY `members`.`id` DESC ", [])['data'][0]['total_count'];

				$fetch_data = [
					"total_count" => process_qry("SELECT COUNT(`members`.`id`) AS `total_count` FROM `members` INNER JOIN `municipality` ON `municipality`.`id` = `members`.`municipality` WHERE `members`.`status` = 1 AND `members`.`deleted_at` IS NULL ORDER BY `members`.`id` DESC ", [])['data'][0]['total_count'],
					"data" => $registrants['data']
				];

				response('success', null, $fetch_data);
			} else {
				response('error', 'No records found');
			}
		}

		public function search_query ($param) {
			
			$query_string = "";
			$query_value = [];

			$param_val = [
				(int) (($param['pagenum']) - 1) * (int) $param['pageitems'],
				(int) $param['pageitems'],
			];

			$column = [
				"`members`.`first_name`",
				"`members`.`middle_name`",
				"`members`.`last_name`",
				"`members`.`email`",
				// "`members`.`birthday`",
				// "`members`.`birthplace`",
				// "`members`.`occupation`",
				"`members`.`religion`",
				// "`members`.`contact`",
			];

			foreach ($column as $key => $value) {
				$query_string .= $value . " LIKE CONCAT (?, '%') OR ";
				array_push($query_value, strtolower($param['search_query']));
			}

			if (isset($param["query_val"]) && !empty($param["query_val"])) {
				$query_string2 = " ) AND `municipality`.`name` = ?";
				array_push($query_value, strtolower($param["query_val"]));
			} else {
				$query_string2 = ")";
			}
			
			$registrants = process_qry("SELECT `members`.`id`, `members`.`first_name`,`members`.`height`, `members`.`weight`, `members`.`spouse_name`, `members`.`num_children` ,`members`.`middle_name`, `members`.`last_name`, `members`.`birthday`, `members`.`birthplace`, `members`.`gender`, `members`.`contact`, `members`.`municipality`, `members`.`address`, `members`.`occupation`, `members`.`religion`, `members`.`civil_status`, `members`.`email`, `members`.`status`, `members`.`verified`, `members`.`agreement`, `members`.`misc`, `municipality`.`name` AS `municipality_name` FROM `members` INNER JOIN `municipality` ON `municipality`.`id` = `members`.`municipality` WHERE (" .rtrim(trim($query_string), "OR") . $query_string2 . " AND `members`.`status` = 1 AND `members`.`deleted_at` IS NULL ORDER BY `members`.`id` DESC LIMIT " . $param_val[0] . ", " . $param_val[1], $query_value);

			if (isset($registrants['response']) && strtolower($registrants['response']) == 'success' && count($registrants['data']) > 0) {
				
				$fetch_data = [
					"pageno" => (int) $param["pagenum"],
					"total_count" => process_qry("SELECT count(*) AS `total_count` FROM `members` INNER JOIN `municipality` ON `municipality`.`id` = `members`.`municipality` WHERE ( " .rtrim(trim($query_string), "OR") . $query_string2 . " AND `members`.`status` = 1 AND `members`.`deleted_at` IS NULL", $query_value)['data'][0]['total_count'],
					"data" => $registrants['data']
				];
				
				response('success', null, $fetch_data);
			} else {
				response('error', "Record(s) not found.");
			}	

		}

		public function municipality_filter ($param) {

			$query_string = "";
			$query_value = [];

			$column = [
				"`members`.`first_name`",
				"`members`.`middle_name`",
				"`members`.`last_name`",
				"`members`.`email`",
				// "`members`.`birthday`",
				// "`members`.`birthplace`",
				// "`members`.`occupation`",
				"`members`.`religion`",
				// "`members`.`contact`",
				// "`municipality`.`name`"
			];

			array_unshift($query_value, $param["query_value"]);

			$param_val = [
				(int) (($param['pagenum']) - 1) * (int) $param['pageitems'],
				(int) $param['pageitems'],
			];

			if (isset($param["search_val"]) && !empty($param["search_val"])) {
				foreach ($column as $key => $value) {
					$query_string .= $value . " LIKE CONCAT (?, '%') OR ";
					array_push($query_value, strtolower($param['search_val']));
				}

				$query_string = "AND (" .rtrim(trim($query_string), "OR") . ")";
			} else {
				$query_string = null;
			}

			$registrants = process_qry("SELECT `members`.`id`, `members`.`first_name`,`members`.`height`, `members`.`weight`, `members`.`spouse_name`, `members`.`num_children` ,`members`.`middle_name`, `members`.`last_name`, `members`.`birthday`, `members`.`birthplace`, `members`.`gender`, `members`.`contact`, `members`.`municipality`, `members`.`address`, `members`.`occupation`, `members`.`religion`, `members`.`civil_status`, `members`.`email`, `members`.`status`, `members`.`verified`, `members`.`agreement`, `members`.`misc`, `municipality`.`name` AS `municipality_name` FROM `members` INNER JOIN `municipality` ON `municipality`.`id` = `members`.`municipality` WHERE  `municipality`.`name` = ? " . $query_string . " AND `members`.`status` = 1 AND `members`.`deleted_at` IS NULL ORDER BY `members`.`id` DESC LIMIT " . $param_val[0] . ", " . $param_val[1], $query_value);
			
			if (isset($registrants['response']) && strtolower($registrants['response']) == 'success' && count($registrants['data']) > 0) {
				
				$fetch_data = [
					"pageno" => (int) $param["pagenum"],
					"total_count" => process_qry("SELECT count(*) AS `total_count` FROM `members` INNER JOIN `municipality` ON `municipality`.`id` = `members`.`municipality` WHERE `municipality`.`name` = ? ". $query_string ." AND `members`.`status` = 1 AND `members`.`deleted_at` IS NULL", $query_value)['data'][0]['total_count'],
					"data" => $registrants['data']
				];

				response('success', null, $fetch_data);
			} else {
				response('error', "Record(s) not found.");
			}

		}

		public function itemsperpage ($param) {


			$query_value = [];
			$query_string2 = "";
			$query_string = "";

			$column = [
				"`members`.`first_name`",
				"`members`.`middle_name`",
				"`members`.`last_name`",
				"`members`.`email`",
				// "`members`.`birthday`",
				// "`members`.`birthplace`",
				// "`members`.`occupation`",
				"`members`.`religion`",
				// "`members`.`contact`",
			];

			$param_val = [
				(int) (($param['pagenum']) - 1) * (int) $param['itemperpage'],
				(int) $param['itemperpage'],
			];

			foreach ($column as $key => $value) {
				$query_string .= $value . " LIKE CONCAT (?, '%') OR ";
				array_push($query_value, strtolower($param['search_val']));
			}

			if (isset($param["municipality"]) && !empty($param["municipality"])) {
				$query_string2 = " ) AND `municipality`.`name` = ?";
				array_push($query_value, strtolower($param["municipality"]));
			} else {
				$query_string2 = ")";
			}
			
			array_push($query_value, $param_val[0], $param_val[1]);

			$registrants = process_qry("SELECT `members`.`id`, `members`.`first_name`,`members`.`height`, `members`.`weight`, `members`.`spouse_name`, `members`.`num_children` ,`members`.`middle_name`, `members`.`last_name`, `members`.`birthday`, `members`.`birthplace`, `members`.`gender`, `members`.`contact`, `members`.`municipality`, `members`.`address`, `members`.`occupation`, `members`.`religion`, `members`.`civil_status`, `members`.`email`, `members`.`status`, `members`.`verified`, `members`.`agreement`, `members`.`misc`, `municipality`.`name` AS `municipality_name` FROM `members` INNER JOIN `municipality` ON `municipality`.`id` = `members`.`municipality` WHERE (" .rtrim(trim($query_string), "OR") . $query_string2 . " AND `members`.`status` = 1 AND `members`.`deleted_at` IS NULL ORDER BY `members`.`id` DESC LIMIT ?,?", $query_value);

			if (isset($registrants['response']) && strtolower($registrants['response']) == 'success' && count($registrants['data']) > 0) { 
				
				$fetch_data = [
					"pageno" => (int) $param["pagenum"],
					"item_size" => $param_val[1],
					"data" => $registrants["data"]
				];

				response('success', null, $fetch_data);
			}
				
		}

		public function register($param) {
			$step_1 = json_decode($this->store($param, 'members'), true);

			if(isset($step_1['response']) && strtolower($step_1['response']) == 'success') {
				response('success','Account succesfully registered.');
			} else {
				response('error', 'Unable to complete process.');
			}
		}

		public function member_migrations() {
			$step_1 = process_qry("SELECT * FROM `wp_cf_form_entry_values`", [])['data'];
			$entry_ids = [];
			// $collection = [];
			$record = [];
			$needed = [
				"family_name",
				"first_name",
				"middle_name",
				"address",
				"birthplace",
				"birthday",
				"occupation",
				"contact_number",
				"religion",
				"gender",
				"civil_status",
				"email_address",
				"agreement",
				"facebook",
			];
			$cs = process_qry("SELECT `id`, `name` FROM `civil_status` WHERE `status` = 1", [])['data'];
			$mun = process_qry("SELECT `id`, `name` FROM `municipality` WHERE `status` = 1", [])['data'];
			$civil_status = [];
			$municipality = [];

			foreach ($cs as $key => $value) {
				$civil_status[$value['name']] = $value['id'];
			}

			foreach ($mun as $key => $value) {
				$municipality[$value['name']] = $value['id'];
			}

			foreach ($step_1 as $key => $value) {
				if (end($entry_ids) != $value['entry_id']) {
					if (!empty($record)) {
						// array_push($collection, $record);
						$this->store($record, 'members');
						$record = [];
					}

					array_push($entry_ids, $value['entry_id']);
				}

				if (in_array($value['slug'], $needed)) {
					if ($value['slug'] == 'contact_number') {
						$record['contact'] = trim( preg_replace( '/[^0-9]/', '', $value['value'] ) );
					} elseif ($value['slug'] == 'email_address') {
						$record['email'] = strtolower( trim( $value['value'] ) );
					} elseif ($value['slug'] == 'agreement') {
						$record['agreement'] = (strpos($value['value'], 'Yes, I Agree!') !== false) ? 1 : 0;
					} elseif ($value['slug'] == 'gender') {
						$record['gender'] = substr( strtolower( trim( $value['value'] ) ), 0, 1);
					} elseif ($value['slug'] == 'civil_status') {
						$record['civil_status'] = (in_array(trim( $value['value'] ), array_keys( $civil_status ))) ? $civil_status[trim( $value['value'] )] : null;
					} elseif ($value['slug'] == 'family_name') {
						$record['last_name'] = trim( $value['value'] );
					} elseif ($value['slug'] == 'facebook') {
						$record['misc'] = trim( $value['value'] );
					} elseif ($value['slug'] == 'address') {
						$record[$value['slug']] = trim( $value['value'] );
						$record['municipality'] = null;
						// $record['municipality'] = (in_array(trim( $value['value'] ), array_keys( $municipality ))) ? $municipality[trim( $value['value'] )] : null;

						foreach ($municipality as $k => $v) {
							if (preg_match("~\b" . strtolower($k) . "\b~", strtolower($value['value']))) {
							// if (strpos(strtolower($value['value']), strtolower($k)) !== false) {
								$record['municipality'] = $v;
								break;
							}
						}
					} else {
						$record[$value['slug']] = trim( $value['value'] );
					}
				}
			}
			
			response('success', 'Migration complete.');
		}
		
		public function update_members($param) {
			$step_1 = json_decode($this->update($param, "members"), true);

			if(isset($step_1['response']) && strtolower($step_1['response']) == 'success') {
				response('success','Account succesfully updated.');
			} else {
				response('error', 'Unable to complete process.');
			}
		}

		public function delete_registrant($param) {
			$step_1 = json_decode($this->delete($param, "members"), true);

			if(isset($step_1['response']) && strtolower($step_1['response']) == 'success') {
				response('success','Account succesfully deleted.');
			} else {
				response('error', 'Unable to complete process.');
			}
		}

		public function store($param, $table) {
			$table = filter_var(trim(urldecode($table)), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
			$step_1 = process_qry("SHOW TABLES LIKE ?", [$table]);

			if (isset($step_1['response']) && strtolower($step_1['response']) === 'success' && count($step_1['data']) > 0) {
				$query_string = "";
				$value_string = "";
				$columns = [];
				$values = [];

				foreach ($param as $key => $value) {
					array_push($columns, $key);
					array_push($values, $value);
				}

				if (count($columns) > 0 && count($values) && (count($columns) == count($values))) {
					$step_2 = process_qry("SHOW COLUMNS FROM `" . $table  . "`", []);

					if (isset($step_2['response']) && strtolower($step_2['response']) === 'success' && count($step_2['data']) > 0) {
						$db_columns = array_column($step_2['data'], 'Field');

						foreach ($columns as $key => $value) {
							if (!in_array($value, $db_columns)) {
								response('error', "Unable to complete the process. Undefined column [$value].");
							}
						}

						$query_string = "INSERT INTO `" . $table . "` (`" . implode("`,`", $columns) . "`, `created_at`, `updated_at`) ";
						$value_string = " VALUES (" . substr(str_repeat("?,", count($values)), 0, -1) . ", NOW(), NOW())";
						$process_qry = process_qry($query_string . $value_string, $values);

						if (isset($process_qry['response']) && strtolower($process_qry['response']) === 'success') {
							// response('success', "Record saved.", process_qry("SELECT `id` FROM `" . $table . "` WHERE  `" . implode("` = ? AND `", $columns) . "` = ? ORDER BY `id` DESC LIMIT 1", $values), null, null, null, 0);
							return json_encode(["response" => "success"]);
						} else {
							response('error', "Unable to insert record.");
						}
					} else {
						response('error', "Unable to complete process. Invalid structure.");
					}
				}
			} else {
				response('error', "Unable to complete process. Undefined table.");
			}
		}

		public function update($param, $table) {
			if (isset($param['id']) && !empty($param['id']) && !empty($table)) {
				$table = filter_var(trim(urldecode($table)), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
				$step_1 = process_qry("SHOW TABLES LIKE ?", [$table]);

				if (isset($step_1['response']) && strtolower($step_1['response']) === 'success' && count($step_1['data']) > 0) {
					$query_string = "";
					$inject = "";
					$columns = [];
					$values = [];

					foreach ($param as $key => $value) {
						if ($key !== 'id') {
							array_push($columns, $key);
							array_push($values, $value);
						}
					}

					if (count($columns) > 0 && count($values) && (count($columns) == count($values))) {
						$step_2 = process_qry("SHOW COLUMNS FROM `" . $table . "`", []);
						if (isset($step_2['response']) && strtolower($step_2['response']) === 'success' && count($step_2['data']) > 0) {
							$db_columns = array_column($step_2['data'], 'Field');

							foreach ($columns as $key => $value) {
								if (!in_array($value, $db_columns)) {
									response('error', "Unable to complete the process. Undefined column [$value].");
								}
							}

							foreach ($columns as $key => $value) {
								$inject .= " `" . $value . "` = ?,";
							}

							array_push($values, $param['id']);

							$step_3 = process_qry("INSERT INTO `revisions` SET `table` = ?, `record_id` = ?, `data` = ?, `status` = 1, `created_at` = NOW()", [$table, $param['id'], json_encode($param)]);

							$query_string = "UPDATE `" . $table . "` SET " . $inject . " `updated_at` = NOW() WHERE `id` = ?";
							$process_qry = process_qry($query_string, $values);

							if (isset($process_qry['response']) && strtolower($process_qry['response']) === 'success') {
								// response('success', "Record updated.", null, null, null, null, 0);
								return json_encode(["response" => "success"]);
							} else {
								response('error', "Unable to update record.");
							}
						} else {
							response('error', "Unable to complete process. Invalid structure.");
						}
					}
				} else {
					response('error', "Unable to complete process. Undefined table.");
				}
			} else {
				response('error', 'Unable to complete process. Insufficient parameter(s).');
			}
		}

		public function delete($param, $table) {
			if (isset($param['id']) && !empty($param['id']) && !empty($table)) {
				$table = filter_var(trim(urldecode($table)), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
				$step_1 = process_qry("SHOW TABLES LIKE ?", [$table]);

				if (isset($step_1['response']) && strtolower($step_1['response']) === 'success' && count($step_1['data']) > 0) {
					$query_string = "UPDATE `" . $table . "` SET `updated_at` = NOW(), `deleted_at` = NOW(), `status` = 0 WHERE `id` = ?";
					$process_qry = process_qry($query_string, [$param['id']]);

					if (isset($process_qry['response']) && strtolower($process_qry['response']) === 'success') {
						response('success', "Record deleted.");
					} else {
						response('error', "Unable to delete record.");
					}
				} else {
					response('error', "Unable to complete process. Undefined table [$table].");
				}
			} else {
				response('error', 'Unable to complete process. Insufficient parameter(s).');
			}
		}

		public function login($param) {
			global $env;

			$default = [
				'username' => null,
				'password' => null,
			];

			$required = [
				'username',
				'password',
			];

			$data = process_params($default, $required, $param);
			$step_1 = process_qry("SELECT * FROM `users` WHERE `name` = ? AND `password` = PASSWORD(?) LIMIT 1", [$data['username'], $data['password']]);

			if (isset($step_1['response']) && strtolower($step_1['response']) === 'success' && count($step_1['data']) > 0) {
				if (!isset($_SESSION)) { session_start(); }

				$_SESSION["web_app_session_id"] = base64_encode(uniqid());
				$_SESSION["web_app_session_un"] = base64_encode($data['username']);
				$_SESSION["web_app_session_pass_key"] = base64_encode(base64_encode($_SESSION["web_app_session_id"]) . '::' . base64_encode($_SESSION["web_app_session_un"]));
				session_write_close();

				response('success', NULL, array(array("web_app_session_id" => $_SESSION["web_app_session_id"], "web_app_session_un" => $_SESSION["web_app_session_un"], "web_app_session_pass_key" => $_SESSION["web_app_session_pass_key"])));
			} else {
				response('error', "Invalid Username and/or Password.");
			}
		}

		public function export_data($param) {
			if ($param['filter'] == "false") {
				$data = process_qry("SELECT * FROM `members`")['data'];
			} else {
				$query_value = [];
				$query_string = "";
				$query_string2 = "";

				if (isset($param['search']) && !empty($param['search'])) {
					$query_string .= "(";
					$column = [
						"`members`.`first_name`",
						"`members`.`middle_name`",
						"`members`.`last_name`",
						"`members`.`email`",
						// "`members`.`birthday`",
						// "`members`.`birthplace`",
						// "`members`.`occupation`",
						"`members`.`religion`",
						// "`members`.`contact`",
					];

					foreach ($column as $key => $value) {
						$query_string .= $value . " LIKE CONCAT ('%', ?, '%') OR ";
						array_push($query_value, strtolower($param['search']));
					}
					$query_string .= ")";
				}

				if (isset($param["municipality"]) && !empty($param["municipality"])) {
					$query_string2 = (isset($param['search']) && !empty($param['search'])) ? " AND `municipality`.`name` = ? AND " : " `municipality`.`name` = ? AND ";
					array_push($query_value, strtolower($param["municipality"]));
				} else {
					$query_string2 = (isset($param['search']) && !empty($param['search'])) ? " AND " : " ";
				}

				$data = process_qry("SELECT `members`.`id`, `members`.`first_name`,`members`.`height`, `members`.`weight`, `members`.`spouse_name`, `members`.`num_children` ,`members`.`middle_name`, `members`.`last_name`, `members`.`birthday`, `members`.`birthplace`, `members`.`gender`, `members`.`contact`, `members`.`municipality`, `members`.`address`, `members`.`occupation`, `members`.`religion`, `members`.`civil_status`, `members`.`email`, `members`.`status`, `members`.`verified`, `members`.`agreement`, `members`.`misc`, `municipality`.`name` AS `municipality_name` FROM `members` INNER JOIN `municipality` ON `municipality`.`id` = `members`.`municipality` WHERE " .rtrim(trim($query_string), "OR") . $query_string2 . " `members`.`status` = 1 AND `members`.`deleted_at` IS NULL ORDER BY `members`.`id` DESC", $query_value)['data'];
			}
			
			$path = realpath(dirname(__FILE__)) . "../../../assets/dump";

			if(!file_exists($path) || !is_dir($path)) { @mkdir($path,0777,true); }

			if (count($data) > 0) {
				$keys = array_keys($data[0]);
				$file_name = uniqid() . '.csv';
				$fp = fopen($path . "/". $file_name, 'wb');

				fputcsv($fp, $keys);

				foreach ($data as $key => $value) {
					$temp = [];
					$temp_keys = array_keys($value);
					foreach ($keys as $k => $v) {
						$temp[] = (in_array($v, $temp_keys)) ? $value[$v] : NULL;
					}

					fputcsv($fp, $temp);
				}

				fclose($fp);
				response('success', null, ['filename' => $file_name]);

				/*if (copy($file_name, $path . "/". $file_name)) {
					response('success', null, ['filename' => $file_name]);
				} else {
					response('error', 'Cannot export files');
				}*/
			} else {
				response('error', 'No data to export.');
			}
		}

		// FRONTEND QUERY PAGE-----

		public function fget_registrants() {

			$registrants = process_qry("SELECT `members`.`id`, `members`.`first_name`,`members`.`height`, `members`.`weight`, `members`.`spouse_name`, `members`.`num_children` ,`members`.`middle_name`, `members`.`last_name`, `members`.`birthday`, `members`.`birthplace`, `members`.`gender`, `members`.`contact`, `members`.`municipality`, `members`.`address`, `members`.`occupation`, `members`.`religion`, `members`.`civil_status`, `members`.`email`, `members`.`status`, `members`.`verified`, `members`.`agreement`, `members`.`misc`, `municipality`.`name` AS `municipality_name` FROM `members` INNER JOIN `municipality` ON `municipality`.`id` = `members`.`municipality` WHERE `members`.`status` = 1 AND `members`.`deleted_at` IS NULL ORDER BY `members`.`id` DESC ", []);

			if (isset($registrants['response']) && strtolower($registrants['response']) == 'success' && count($registrants['data']) > 0) {

				response('success', null, $registrants['data']);
			} else {
				response('error', 'No records found');
			}
		}

		public function get_member_count() {
			$members = process_qry("SELECT COUNT(*) AS `count` FROM `members` WHERE `status` = 1 AND `deleted_at` IS NULL;", [])['data'][0]['count'];
			$output = [
				'municipality' => process_qry("SELECT COUNT(DISTINCT(`municipality`)) AS `count` FROM `members` WHERE `status` = 1 AND `deleted_at` IS NULL AND `municipality` IS NOT NULL;", [])['data'][0]['count'],
				'members' => (($members < 20000) ? 20000 + ((int) floor($members / 100)) : $members),
			];
			response('success', null, $output);
		}

	}

?>