<?php

	require_once((($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']) . '/php/config/config.php');

	if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
		redirect($base_domain, true, 'Warning - Invalid Access', $base, array(array("_POST" => $_POST, "_GET" => $_GET)));
	}

	class log extends JWT {
		private $cipher;
		private $storage;

		public function __construct() {
			global $env;
			global $base;
			$this->cipher = new cipher();
			$this->storage = $base . '/php/tools/logs/archive/';
		}

		public function aes_decrypt($param) {
			response('success', NULL, array('aes'=>$this->cipher->decrypt(urldecode($param['aes']))));
		}

		public function jwt_decode($param) {
			global $env;
			$param['jwt'] = str_replace('Bearer ', '', urldecode($param['jwt']));
			$jwt_arr = $this->decode($param['jwt'], $env->jwt_ssk);

			if (is_object($jwt_arr)) {
				response('success', NULL, array('jwt'=>$jwt_arr));
			} else {
				response('error', "Invalid segment encoding.");
			}
		}

		public function date_folder() {
			$folders = scandir($this->storage);
			$dd_folders = "<option value='NULL'>Select a Folder</option>";
			rsort($folders);

			foreach ($folders as $folder) {
				$dd_folders .= ((($folder != '.') && ($folder != '..')) && (strpos($folder, '.php') === false)) ? "<option value='" . $folder . "'>" . $folder . "</option>" : NULL;
			}

			response('success', NULL, array('option'=>$dd_folders));
		}

		public function get_folder_data($param) {
			$default = array('folder_name'=>NULL);
			$required = array('folder_name');
			$data = process_params($default, $required, $param);
			$dir = $this->storage . $data['folder_name'];
			$dd_files = "<option value='NULL'>-- Select a File --</option>";
			$raw = NULL;
			$sanitized = NULL;
			$response = NULL;

			if (is_dir($dir)) {
				$files = scandir($dir);
				sort($files);

				foreach ($files as $file) {
					if (strpos($file, '.txt') !== false) {
						if (strpos($file, 'RAW') !== false) {
							$raw .= "<option value='" . $file . "'>" . ucwords(str_replace(array('.txt', 'RAW - ', '_'), array('', '', ' '), $file)) . "</option>";
						} elseif (strpos($file, 'Sanitized') !== false) {
							$sanitized .= "<option value='" . $file . "'>" . ucwords(str_replace(array('.txt', 'Sanitized - ', '_'), array('', '', ' '), $file)) . "</option>";
						} elseif (strpos($file, 'Response') !== false) {
							$file_name = trim(ucwords(str_replace(array('.txt', 'Response - ', '_'), array('', '', ' '), $file)));
							$response .= "<option value='" . $file . "'>" . ((!$file_name) ? 'Invalid' : $file_name) . "</option>";
						}
					}
				}

				if (!empty($raw)) { $dd_files .= "<optgroup label='RAW'>" . $raw . "</optgroup>"; }
				if (!empty($sanitized)) { $dd_files .= "<optgroup label='SANITIZED'>" . $sanitized . "</optgroup>"; }
				if (!empty($response)) { $dd_files .= "<optgroup label='RESPONSE'>" . $response . "</optgroup>"; }
			}

			response('success', NULL, array('option'=>$dd_files));
		}

		public function get_file_data($param) {
			$default = array('folder_name'=>NULL, 'file_name'=>NULL);
			$required = array('folder_name', 'file_name');
			$data = process_params($default, $required, $param);
			$dir = (strpos(urldecode($data['file_name']), '.txt') !== false) ? $this->storage . $data['folder_name'] . '/' . urldecode($data['file_name']) : $this->storage . $data['folder_name'] . '/' . urldecode($data['file_name']) . '.txt';
			$output = array();

			if (file_exists($dir)) {
				$logs = explode("\r\n", trim(file_get_contents($dir)));

				foreach ($logs as $log) {
					$log_arr = json_decode($log);
					$log = array();

					foreach ($log_arr as $key => $value) {
						if ($key === 'data') {
							$log[$key] = json_encode($value);
						} else {
							$log[$key] = $value;
						}
					}

					array_push($output, $log);
				}
			}

			response('success', NULL, $output);
		}
	}

?>