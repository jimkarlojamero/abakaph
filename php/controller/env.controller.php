<?php

	require_once((($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']) . '/php/config/config.php');

	if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
		redirect($base_domain, true, 'Warning - Invalid Access', $base, array(array("_POST" => $_POST, "_GET" => $_GET)));
	}

	class env {
		public function __construct() {
			global $base;
			$env_file = NULL;

			while (!file_exists($base . '/.env')) {
				$this->generate_env([], 0);
			}
		}

		public function parse_env($param = []) {
			global $base;
			$file_path = $base . "/.env";

			if (!file_exists($file_path)) {
				response('error', 'Unable to locate .env file.');
			}

			$env_file = file($file_path);
			$env_arr = array();
			$connection = array();

			foreach ($env_file as $line) {
				array_push($env_arr, $line);
			}

			if (count($env_arr) == 2) {
				$connection = json_decode(base64_decode($env_arr[1]), true);
				$env_step_1 = explode('<br />', base64_decode($env_arr[0]));

				if (count($env_step_1) == 4) {
					$env_step_2 = array();

					foreach ($env_step_1 as $node) {
						$temp_node = explode('::', base64_decode($node));

						if (array_key_exists($temp_node[0], $connection)) {
							$connection[$temp_node[0]] = $temp_node[1];
						}
					}
				}
			}

			if (ENV_STATUS === 0 && !isset($param['generate'])) {
				unset($connection['host']);
				unset($connection['un']);
				unset($connection['pw']);
				unset($connection['db']);
				unset($connection['port']);
			}

			$connection['env_status'] = ENV_STATUS;
			
			return $connection;
		}

		public function generate_env($param = [], $terminate = 1) {
			global $base;
			global $env_config;

			$default = [];
			$required = []; // keys only

			foreach ($env_config as $key => $value) {
				$default[$key] = (isset($value['default'])) ? $value['default'] : null;
			}

			if (ENV_STATUS == 0) {
				$default = $this->parse_env(array('generate' => 1));
				foreach ($required as $node) { $param[$node] = $default[$node]; }
			}
			
			$keys = array_keys($default);
			$data = array_merge($default, $param);
			$array_diff = array_values(array_diff($required, array_keys($param)));
			$env = NULL;

			if (count($array_diff) > 0) {
				response('error', "Parameter ( " . $array_diff[0] . " ) is required.");
			}

			foreach ($param as $key => $value) {
				if (!in_array($key, $keys)) {
					response('error', "Invalid parameter ( " . $key . " )", array($param));
				}

				$env .= base64_encode($key . "::" . $value) . "<br />";
			}

			$env = base64_encode(substr($env, 0, -6)) . PHP_EOL . base64_encode(json_encode($data));
			$fhandler = fopen($base . "/.env", "w");

			fwrite($fhandler, $env);
			fclose($fhandler);
			generate_htaccess($data['domain_name']);
			response('success', ".env Generation completed.", NULL, NULL, NULL, NULL, $terminate);
		}

		public function login($param) {
			global $env;
			$default = array('username'=>NULL, 'password'=>NULL);
			$required = array('username', 'password');
			$data = process_params($default, $required, $param);

			if ($env->adm_un === $data['username'] && $env->adm_pw === $data['password']) {
				if (!isset($_SESSION)) { session_start(); }

				$_SESSION["env_session_id"] = base64_encode(uniqid());
				$_SESSION["env_session_un"] = base64_encode($data['username']);
				$_SESSION["env_session_pass_key"] = base64_encode(base64_encode($_SESSION["env_session_id"]) . '::' . base64_encode($_SESSION["env_session_un"]));

				session_write_close();
				response('success', NULL, array(array("env_session_id" => $_SESSION["env_session_id"], "env_session_un" => $_SESSION["env_session_un"], "env_session_pass_key" => $_SESSION["env_session_pass_key"])));
			} else {
				response('error', "Invalid Username/Password.");
			}
		}

	}

?>