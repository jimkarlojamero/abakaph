<?php

	require_once((($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']) . '/php/config/config.php');

	if (basename($_SERVER['PHP_SELF']) == basename(__FILE__) || LOGS_STATUS !== 1) { redirect($base_domain); }
	if (!isset($_SESSION)) { session_start(); }
	if (isset($_SESSION['env_session_id']) && isset($_SESSION['env_session_un']) && isset($_SESSION['env_session_pass_key'])) {
		if (base64_encode(base64_encode($_SESSION["env_session_id"]) . '::' . base64_encode($_SESSION["env_session_un"])) != $_SESSION['env_session_pass_key']) {
			define('SESSION_STATUS', 0);
			unset($_SESSION["env_session_id"]);
			unset($_SESSION["env_session_un"]);
			unset($_SESSION["env_session_pass_key"]);
		} else { define('SESSION_STATUS', 1); }
	} else { define('SESSION_STATUS', 0); }

?>

<!DOCTYPE html>
<html lang="en" ng-app="ITC" oncontextmenu="return false">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?php echo ($env->app_name) ? $env->app_name : "Info Crowd Tech"; ?></title>

	<script type="text/javascript">
		var base_domain = "<?php echo $base_domain; ?>";
		var is_login = <?php echo SESSION_STATUS; ?>;
		var global_flag = <?php echo ENV_STATUS; ?>;
	</script>

	<link rel="icon" type="image/png" href="<?php echo $base_domain . '/assets/images/logo.png?v=' . DATE('is'); ?>" onerror="this.href='<?php echo $base_domain ?>/assets/images'" />
	<link href="<?php echo $base_domain ?>/assets/css/bootstrap.min.css?v=<?php echo DATE('is')?>" rel="stylesheet">
	<link href="<?php echo $base_domain ?>/assets/css/toastr.min.css?v=<?php echo DATE('is')?>" rel="stylesheet">
	<link href="<?php echo $base_domain ?>/assets/css/preloader.css?v=<?php echo DATE('is')?>" rel="stylesheet">
	<link href="<?php echo $base_domain ?>/assets/css/animate.css?v=<?php echo DATE('is')?>" rel="stylesheet">
	<link href="<?php echo $base_domain ?>/assets/css/styles.css?v=<?php echo DATE('is')?>" rel="stylesheet">
<?php if (SESSION_STATUS === 0) { ?>
	<link href="<?php echo $base_domain ?>/assets/css/floating-labels.css?v=<?php echo DATE('is')?>" rel="stylesheet">
<?php } if (strpos($_SERVER['PHP_SELF'], 'docs') !== false) { ?>
	<link href="<?php echo $base_domain ?>/assets/css/syntax.css?v=<?php echo DATE('is')?>" rel="stylesheet">
<?php } else if (strpos($_SERVER['PHP_SELF'], 'logs') !== false) { ?>
	<link href="<?php echo $base_domain ?>/assets/css/json-viewer.css?v=<?php echo DATE('is')?>" rel="stylesheet">
<?php } ?>
	
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css?v=<?php echo DATE('is')?>">
</head>
<body class="m-0 p-0">
	<div id="preloader" class="animated fadeIn">
		<div id="preview-area" class="minipreloader-wrapper">
			<div class="spinner">
				<div class="dot1"></div>
				<div class="dot2"></div>
			</div>
		</div>
	</div>