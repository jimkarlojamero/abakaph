<?php

	require_once((($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']) . '/php/config/config.php');

	if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) { redirect($base_domain); }

?>

	<p class="fixed-bottom mt-5 mb-0 pb-1 pt-1 text-center bg-dark text-light">&copy; <?php echo DATE('Y'); ?> ICT</p>

</body>

	<script type="text/javascript" src="<?php echo $base_domain ?>/assets/js/jquery.min.js?v=<?php echo DATE('is')?>"></script>
	<script type="text/javascript" src="<?php echo $base_domain ?>/assets/js/toastr.min.js?v=<?php echo DATE('is')?>"></script>
	<script type="text/javascript" src="<?php echo $base_domain ?>/assets/js/popper.min.js?v=<?php echo DATE('is')?>"></script>
	<script type="text/javascript" src="<?php echo $base_domain ?>/assets/js/bootstrap.min.js?v=<?php echo DATE('is')?>"></script>
<?php if (strpos($_SERVER['PHP_SELF'], 'env') !== false) { ?>
	<script type="text/javascript" src="<?php echo $base_domain ?>/assets/js/env.js?v=<?php echo DATE('is')?>"></script>
<?php } else { ?>
	<script type="text/javascript" src="<?php echo $base_domain ?>/assets/js/angular.min.js?v=<?php echo DATE('is')?>"></script>
	<script type="text/javascript" src="<?php echo $base_domain ?>/assets/js/dirpagination.js?v=<?php echo DATE('is')?>"></script>
	<script type="text/javascript" src="<?php echo $base_domain ?>/assets/js/json-viewer.js?v=<?php echo DATE('is')?>"></script>
	<script type="text/javascript" src="<?php echo $base_domain ?>/assets/js/logs.js?v=<?php echo DATE('is')?>"></script>
<?php } ?>
	<script type="text/javascript">
		$(document).keydown(function(e){
			if (
				(e.which === 123) // f12
				|| (e.ctrlKey && (e.keyCode === 83)) // ctrl + s
				|| (e.ctrlKey && (e.keyCode === 85)) // ctrl + u
				|| (e.shiftKey && (e.which === 113)) // shift + f2
				|| (e.shiftKey && (e.which === 115)) // shift + f4
				|| (e.shiftKey && (e.which === 116)) // shift + f5
				|| (e.shiftKey && (e.which === 118)) // shift + f7
				|| (e.shiftKey && (e.which === 119)) // shift + f8
				|| (e.shiftKey && (e.which === 120)) // shift + f9
				|| (e.ctrlKey && e.shiftKey && (e.keyCode === 73)) // ctrl + shift + i
				|| (e.ctrlKey && e.shiftKey && (e.keyCode === 67)) // ctrl + shift + c
				|| (e.ctrlKey && e.shiftKey && (e.keyCode === 75)) // ctrl + shift + k
				|| (e.ctrlKey && e.shiftKey && (e.keyCode === 83)) // ctrl + shift + s
				|| (e.ctrlKey && e.shiftKey && (e.keyCode === 69)) // ctrl + shift + e
				|| (e.ctrlKey && e.shiftKey && (e.keyCode === 74)) // ctrl + shift + j
				|| (e.ctrlKey && e.shiftKey && (e.keyCode === 77)) // ctrl + shift + m
			) {
				return false;
			}
		});
	</script>

</html>