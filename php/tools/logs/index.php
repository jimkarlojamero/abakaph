<?php

	$base = (($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']);
	require_once($base . '/php/tools/inc/header.php');

	if (SESSION_STATUS === 0) {
?>

	<div class="form-signin">
		<div class="text-center mb-4">
			<img class="mb-4 rounded" src="<?php echo $base_domain . '/assets/images/logo.png?v=' . DATE('is'); ?>" onerror="this.src='<?php echo $base_domain ?>/assets/images'" id="imageLogo" alt="" width="72" height="72">
			<h1 class="h3 mb-3 font-weight-normal"><?php echo ($env->app_name) ? $env->app_name : "Info Crowd Tech"; ?></h1>
			<code>AUTHORIZED ACCESS ONLY</code>
			<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <code>Integer congue neque placerat</code> , feugiat nisi at, tincidunt tellus.</p> -->
		</div>

		<div class="form-label-group">
			<input type="text" id="username" class="form-control" placeholder="Username" required autofocus />
			<label for="username">Username</label>
		</div>

		<div class="form-label-group">
			<input type="password" id="password" class="form-control" placeholder="Password" required />
			<label for="password">Password</label>
		</div>

		<button class="btn btn-lg btn-primary btn-block" type="submit" id="login_btn">Sign in</button>
	</div>

<?php } else { ?>
	<!-- Image and text -->
	<nav class="navbar navbar-light bg-light">
		<a class="navbar-brand" href="<?php echo $base_domain ?>/php/tools/logs">
			<img src="<?php echo $base_domain . '/assets/images/logo.png?v=' . DATE('is'); ?>" onerror="this.src='<?php echo $base_domain ?>/assets/images'" width="30" height="30" class="d-inline-block align-top" alt="">
			<?php echo ($env->app_name) ? $env->app_name : "Info Crowd Tech"; ?>
		</a>
		<div class="form-inline">
			<button class="btn btn-outline-secondary mr-1" data-toggle="modal" data-target="#decrypt_modal" id="b_aes_modal">AES</button>
			<button class="btn btn-outline-secondary mr-1" data-toggle="modal" data-target="#decrypt_modal" id="b_jwt_modal">JWT</button>
			<button class="btn btn-outline-dark" id="sign_out_btn"><i class="fa fa-sign-out"></i></button>
		</div>
	</nav>

	<div class="container-fluid">
		<div class="row">
			<main role="main" class="col-md-12 ml-sm-auto col-lg-12 pt-3 px-4" id="conaiter_logs" ng-controller="controller_logs" ng-init="view_logs()">
				<div class="form-group">
					<div class="form-row">
						<div class="col">
							<label for="date_folder">Date(s):</label>
							<select id="date_folder" class="form-control form-control-sm"></select>
						</div>
						<div class="col">
							<label for="log_file">Log Type:</label>
							<select id="log_file" class="form-control form-control-sm">
								<option>No Option(s) Available</option>
							</select>
						</div>
						<div class="col">
							<label for="search">Item(s) per Page:</label>
							<input type="number" min="1" max="100" class="form-control form-control-sm" ng-model="logs_page_size">
						</div>
						<div class="col">
							<label for="search">Search:</label>
							<input ng-model="search_logs" id="search" class="form-control form-control-sm" placeholder="Filter text">
						</div>
					</div>
				</div>

				<table class="table table-dark table-striped table-hover table-sm" id="logs_table">
					<thead>
						<tr>
							<th>ID</th>
							<th>IP</th>
							<th>Time Stamp</th>
							<th>File</th>
							<th>Data</th>
						</tr>
					</thead>
					<tbody style="display: none;">
						<tr dir-paginate="log in logs | filter:search_logs:false | orderBy:'date':true | itemsPerPage: logs_page_size" current-page="currentPage" pagination-id="pagination_logs">
							<td class="align-middle">{{ log.id }}</td>
							<td class="align-middle">{{ log.client }}</td>
							<td class="align-middle">{{ log.timestamp }}</td>
							<td class="align-middle">{{ log.file }}</td>
							<td class="align-middle"><button type="button" class="btn btn-sm btn-outline-light" ng-click="show_data( log.data, search_logs )">View Data</button></td>
						</tr>
					</tbody>
				</table>

				<div ng-controller="pagination_logs">
					<div class="text-center">
						<dir-pagination-controls boundary-links="true" on-page-change="logs_pages(newPageNumber)" template-url="<?php echo $base_domain ?>/php/misc/dirpagination.tpl.php" pagination-id="pagination_logs"></dir-pagination-controls>
					</div>
				</div>
			</main>
		</div>
	</div>

	<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="result_modal_title" aria-hidden="true" id="result_modal">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="result_modal_title"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form">
						<div class="form-group bg-light border pt-3" id="pre_content" scroll="no"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="decrypt_modal" tabindex="-1" role="dialog" aria-labelledby="decrypt_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="decrypt_modal_label"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="textarea_label"></span>
						</div>
						<textarea class="form-control" id="encrypted_value" rows="5" autofocus></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="process_request"></button>
				</div>
			</div>
		</div>
	</div>

<?php } require_once($base . '/php/tools/inc/footer.php'); ?>