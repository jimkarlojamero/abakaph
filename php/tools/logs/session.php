<?php

	require_once((($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']) . '/php/config/config.php');

	if (!isset($_SESSION)) { session_start(); }

	if (isset($_GET['env_session_id']) && isset($_GET['env_session_un']) && isset($_GET['env_session_pass_key'])) {
		if (base64_encode(base64_encode($_GET["env_session_id"]) . '::' . base64_encode($_GET["env_session_un"])) != $_GET['env_session_pass_key']) {
			define('SESSION_STATUS', 0);
			unset($_SESSION["env_session_id"]);
			unset($_SESSION["env_session_un"]);
			unset($_SESSION["env_session_pass_key"]);
			header('Location: ' . $base_domain . '/php/tools/logs');
			exit();
		} else {
			define('SESSION_STATUS', 1);
			$_SESSION['env_session_id'] = $_GET['env_session_id'];
			$_SESSION['env_session_un'] = $_GET['env_session_un'];
			$_SESSION['env_session_pass_key'] = $_GET['env_session_pass_key'];
			session_write_close();
			header('Location: ' . $base_domain . '/php/tools/logs');
			exit();
		}
	} else {
		unset($_SESSION["env_session_id"]);
		unset($_SESSION["env_session_un"]);
		unset($_SESSION["env_session_pass_key"]);
		header('Location: ' . $base_domain . '/php/tools/logs');
		exit();
	}

?>