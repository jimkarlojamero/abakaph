<?php

	$base = (($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']);
	require_once($base . '/php/tools/inc/header.php');

	if (SESSION_STATUS === 0) { 
?>

	<div class="form-signin">
		<div class="text-center mb-4">
			<img class="mb-4 rounded" src="<?php echo $base_domain . '/assets/images/logo.png?v=' . DATE('is'); ?>" onerror="this.src='<?php echo $base_domain ?>/assets/images'" id="imageLogo" alt="" width="72" height="72">
			<h1 class="h3 mb-3 font-weight-normal"><?php echo ($env->app_name) ? $env->app_name : "Info Crowd Tech"; ?></h1>
			<code>AUTHORIZED ACCESS ONLY</code>
			<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <code>Integer congue neque placerat</code> , feugiat nisi at, tincidunt tellus.</p> -->
		</div>

		<div class="form-label-group">
			<input type="text" id="username" class="form-control" placeholder="Username" required autofocus />
			<label for="username">Username</label>
		</div>

		<div class="form-label-group">
			<input type="password" id="password" class="form-control" placeholder="Password" required />
			<label for="password">Password</label>
		</div>

		<button class="btn btn-lg btn-primary btn-block" type="submit" id="login_btn">Sign in</button>
	</div>

<?php } else { ?>

	<nav class="navbar navbar-light bg-light">
		<a class="navbar-brand" href="env_mngr.php">
			<img src="<?php echo $base_domain . '/assets/images/logo.png?v=' . DATE('is'); ?>" onerror="this.src='<?php echo $base_domain ?>/assets/images'" width="30" height="30" class="d-inline-block align-top" alt="">
			<?php echo ($env->app_name) ? $env->app_name : "Info Crowd Tech"; ?>
		</a>
		<div class="form-inline">
			<button class="btn btn-outline-dark mr-1" id="sign_out_btn"><i class="fa fa-sign-out"></i></button>
		</div>
	</nav>

	<div class="container_fluid">
		<main role="main" class="col-md-12 ml-sm-auto col-lg-12 pt-3 px-4">
			<form class="needs-validation" id="environment" novalidate>
				<div class="form-row">
					<?php foreach ($env_config as $key => $values) { ?>
						<div class="col-md-3 mb-3">
							<label for="<?php echo $key; ?>">
								<?php echo (is_array(($env_config[$key])) && isset($env_config[$key]['title'])) ? $env_config[$key]['title'] : ucwords(strtolower(str_replace('_', ' ', $env_config[$key]))); ?>
							</label>
							<?php
								$value = (isset($env->$key)) ? $env->$key : ((isset($env_config[$key]['default'])) ? $env_config[$key]['default'] : null);
								$placeholder = (isset($env_config[$key]['placeholder'])) ? $env_config[$key]['placeholder'] : "Please enter " . ucwords(strtolower(str_replace('_', ' ', $env_config[$key])));
								$type = (isset($env_config[$key]['type'])) ? $env_config[$key]['type'] : 'text';
								$plugin = (isset($env_config[$key]['plugin'])) ? 'data-plugin="' . $env_config[$key]['plugin'] . '" data-' . $env_config[$key]['plugin'] . '="' . ((isset($env_config[$key]['length'])) ? $env_config[$key]['length'] : 16) . '"' : null;
								$required = (isset($env_config[$key]['required']) && $env_config[$key]['required'] === true) ? 'required' : null;

								if ($plugin) {
									echo '<div class="input-group">';
								}

								echo '<input type="' . $type . '" class="form-control font-weight-bold text-primary" id="' . $key . '" name="' . $key . '" value="' . $value . '" placeholder="' . $placeholder . '" ' . $plugin . ' ' . $required . '>';

								if ($plugin) {
									echo '<div class="input-group-append"><button class="input-group-text btn btn-outline-success" data-target="' . $key . '"><i class="fa fa-gears"></i></button></div></div>';
								}
							?>
						</div>
					<?php } ?>
				</div>

				<button class="btn btn-outline-primary btn-block">Generate</button>
			</form>

			<hr />

			<div class="needs-validation" novalidate>
				<h4 class="text-center">Test Push</h4>

				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="push_id">Push ID</label>
						<input type="text" class="form-control font-weight-bold text-primary" id="push_id" placeholder="[Th1s_1s-@-uN1qu3_#]" required>
						<small id="emailHelp" class="form-text text-muted">In-order to send to multiple devices, just add a <code data-toggle="tooltip" data-placement="bottom" title="push_id_1,push_id_2">comma</code> ( <code>,</code> ) as a delimeter.</small>
					</div>
					<div class="col-md-6 mb-3">
						<label for="push_msg">Message</label>
						<input type="text" class="form-control font-weight-bold text-primary" id="push_msg" placeholder="[This is only a test!]" required>
					</div>
				</div>

				<button class="btn btn-outline-primary btn-block" id="s_push">Send</button>
			</div>
		</main>
	</div>

<?php } require_once($base . '/php/tools/inc/footer.php'); ?>