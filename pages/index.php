<?php

    require_once((($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']) . '/php/config/config.php');

?>
<!doctype html>
<html lang="en" ng-app="mainapp" ng-cloak>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="token" content="<?php echo (!isset($_SESSION['web_app_session_pass_key']))  ? null : $_SESSION['web_app_session_pass_key']; ?>">
    <link rel="icon" href="">

    <title>Abaka</title>

    <link rel="icon" type="image/png" href="<?php echo $base_domain . '/assets/images/logo.png?v=' . DATE('is'); ?>" onerror="this.href='<?php echo $base_domain ?>/assets/images'" />

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/all.min.css" rel="stylesheet">
    <link href="assets/css/line-awesome.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/preloader.css" rel="stylesheet">
    <link href="assets/css/toastr.min.css" rel="stylesheet">
    <link href="assets/css/sweetalert2.min.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">
    <link href="assets/css/styles.css" rel="stylesheet">
  </head>
  <body class="bg-light">
    <div id="preloader" class="animated fadeIn">
        <div id="preview-area" class="minipreloader-wrapper">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
    </div>

    <div class="container">

      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4 rounded" src="assets/images/logo.png" onerror="this.src='assets/images'" alt="" width="100" height="100">
        <h2>Abaka PH</h2>
        <p class="lead">We are a community organization and a voluntary movement for all Boholanos to participate in the grassroots campaign for true and lasting change. We are a partner of Hugpong ng Pagbabago (HNP) of Mayor Sara Duterte-Carpio and therefore supportive of whatever programs and activities that the Party may initiate towards nation-building where the benefits are intended for all.</p>
      </div>

      <div class="row">
        <div class="col-12">
          <h4 class="mb-3">Registration Form</h4>
          <form id="test-form" class="needs-validation mb-5" novalidate>
            <div class="row">
              <div class="col-md-4 mb-3">
                <label for="first_name">First Name</label>
                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="" autofocus >
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-4 mb-3">
                <label for="middle_name">Middle Name</label>
                <input type="text" class="form-control" id="middle_name" name="middle_name" placeholder="Middle Name" value="">
              </div>
              <div class="col-md-4 mb-3">
                <label for="last_name">Last Name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="" >
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
              <div class="col-md-4 mb-3">
                <label for="gender">Gender</label>
                <select class="custom-select d-block w-100" id="gender" name="gender" >
                  <option value="">Choose...</option>
                  <option value="m">Male</option>
                  <option value="f">Female</option>
                </select>
                <div class="invalid-feedback">
                  Please provide a valid gender.
                </div>
              </div>
              <div class="col-md-4 mb-3">
                <div class="row">
                  <div class="col">
                    <label for="height">Height</label>
                    <input type="text" class="form-control" id="height" name="height" placeholder="Height" value="">
                  </div>
                  <div class="col">
                    <label for="weight">Weight</label>
                    <input type="text" class="form-control" id="weight" name="weight" placeholder="Weight" value="">
                  </div>
                </div>
              </div>
              <div class="col-md-4 mb-3">
                <label for="birthday">Birthday</label>
                <input type="date" class="form-control" id="birthday" name="birthday" placeholder="" value="" >
                <div class="invalid-feedback">
                  Valid birthday is required.
                </div>
              </div>
              <div class="col-md-4 mb-3">
                <label for="birthplace">Birth Place</label>
                <input type="text" class="form-control" id="birthplace" name="birthplace" placeholder="Birthplace" value="">
              </div>
              <div class="col-md-4 mb-3">
                <label for="civil_status">Civil Status</label>
                <select class="custom-select d-block w-100" id="civil_status" name="civil_status" >
                  <option value="">Choose...</option>
                </select>
                <div class="invalid-feedback">
                  Please provide a valid civil status.
                </div>
              </div>
              <div class="col-md-4 mb-3">
                <label for="religion">Religion</label>
                <input type="text" class="form-control" id="religion" name="religion" placeholder="Religion" value="">
              </div>
              <div class="col-md-4 mb-3">
                <label for="occupation">Occupation</label>
                <input type="text" class="form-control" id="occupation" name="occupation" placeholder="Occupation" value="">
              </div>
              <div class="col-md-4 mb-3">
                <label for="spouse_name">Name of Spouse</label>
                <input type="text" class="form-control" id="spouse_name" name="spouse_name" placeholder="Name of spouse" value="">
              </div>
              <div class="col-md-4 mb-3">
                <label for="num_children">No. of Children</label>
                <input type="number" class="form-control" id="num_children" name="num_children" placeholder="Number of children" min='0' value="">
              </div>
              <div class="col-md-4 mb-3">
                <label for="contact">Contact</label>
                <input type="text" class="form-control" id="contact" name="contact" placeholder="****-***-****" value="">
              </div>
              <div class="col-md-4 mb-3">
                <label for="email">E-Mail</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="you@example.com" value="">
                <div class="invalid-feedback">
                  Valid email is required.
                </div>
              </div>
              <div class="col-md-4 mb-3">
                <label for="municipality">Municipality</label>
                <select class="custom-select d-block w-100" id="municipality" name="municipality" required>
                  <option value="">Choose...</option>
                </select>
                <div class="invalid-feedback">
                  Please provide a valid municipality.
                </div>
              </div>
            </div>

            <div class="mb-3">
              <label for="address">Address</label>
              <textarea type="text" class="form-control" id="address" name="address" placeholder="1234 St., XYZ City" style="resize:none;" ></textarea>
              <div class="invalid-feedback">
                Please enter your address.
              </div>
            </div>

            <div class="mb-3">
                <label for="misc">Extra Inormation</label>
                <textarea type="text" class="form-control" id="misc" name="misc" value="" placeholder="Extra information" style="resize:none;"></textarea>
            </div>

            <hr class="mb-4">

            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="agreement" name="agreement" required>
              <label class="custom-control-label" for="agreement"><b>Yes, I Agree!</b> As a member of ABAKA, you must support Mayor Sara Duterte's vision which ABAKA adheres to, of a strong Province providing a secure life for all Boholanos, good governance and effective leadership. You must also support the shift towards federalism and PRRD's programs towards the eradication of graft and corruption, drugs and criminality that plague our society today.</label>
            </div>

            <hr class="mb-4">
            <button class="btn btn-primary btn-lg pl-5 pr-5 btn-outline-dark" type="submit">Register</button>
          </form>
        </div>
      </div>
    </div>
   
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="assets/js/holder.min.js"></script>
    <script src="assets/js/toastr.min.js"></script>
    <script src="assets/js/sweetalert2.min.js"></script>
    <script src="assets/js/scripts.js"></script>
    <script src="assets/js/test-form.js"></script>
  </body>
</html>