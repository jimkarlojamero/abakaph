<?php

	$base = (($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']);
	require_once($base . '/pages/inc/header.php');

	if (WEB_APP_SESSION_STATUS === 0) {
?>

	<form id='login' class="form-signin">
		<div class="text-center mb-4">
			<img class="mb-4 rounded" src="<?php echo $base_domain . '/assets/images/logo.png?v=' . DATE('is'); ?>" onerror="this.src='<?php echo $base_domain ?>/assets/images'" id="imageLogo" alt="" width="72" height="72">
			<h1 class="h3 mb-3 font-weight-normal"><?php echo ($env->app_name) ? $env->app_name : "Info Crowd Tech"; ?></h1>
			<code>AUTHORIZED ACCESS ONLY</code>
			<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <code>Integer congue neque placerat</code> , feugiat nisi at, tincidunt tellus.</p> -->
		</div>

		<div class="form-label-group">
			<input type="text" id="username" name="username" class="form-control" placeholder="Username" required autofocus />
			<label for="username">Username</label>
		</div>

		<div class="form-label-group">
			<input type="password" id="password"  name="password" class="form-control" placeholder="Password" required />
			<label for="password">Password</label>
		</div>

		<button class="btn btn-lg btn-primary btn-block" type="submit" id="login_btn">Sign in</button>
	</form id='login'>

<?php } else { ?>
	<!-- Image and text -->
	<nav class="navbar navbar-light bg-light">
		<a class="navbar-brand" href="<?php echo $base_domain ?>/php/tools/logs">
			<img src="<?php echo $base_domain . '/assets/images/logo.png?v=' . DATE('is'); ?>" onerror="this.src='<?php echo $base_domain ?>/assets/images'" width="30" height="30" class="d-inline-block align-top" alt="">
			<?php echo ($env->app_name) ? $env->app_name : "Info Crowd Tech"; ?>
		</a>
		<div class="form-inline">
			<button class="btn btn-link text-decoration-none font-weight-bolder text-dark" id="sign_out_btn">Logout</button>
		</div>
	</nav>

	<div class="container-fluid mb-5">
		<div class="row">
			<main role="main" class="col-md-12 ml-sm-auto col-lg-12 pt-3 px-4" id="container" ng-controller="front-controller" ng-init="view_data()">
				<div class="form-group">
					<div class="form-row">
						<div class="col">
							<label for="fmunicipality">Municipality:</label>
							<select class="form-control form-control-sm municipality searchable-filter" id="fmunicipality" ng-model="fmunicipality"></select>
						</div>
						<div class="col">
							<label for="fitems_per_page">Item(s) per Page:</label>
							<!-- <input type="number" min="1" max="100" class="form-control form-control-sm searchable-filter"> -->
							<input type="number" min="1" max="100" class="form-control form-control-sm searchable-filter" id="fitems_per_page">
						</div>
						<div class="col">
							<label for="fsearch">Search:</label>
							<input id="fsearch" ng-model="search.filter" class="form-control form-control-sm searchable-filter" placeholder="Search name, email, religion..." >
						</div>
						<div class="col-1">
							<label for="export">&nbsp;</label>
							<button class="form-control form-control-sm btn btn-sm btn-outline-dark" id="export"><i class="la la-refresh mr-1"></i>Export</button>
						</div>
						<!-- <div class="col-1">
							<label for="reset-search">&nbsp;</label>
							<button class="form-control form-control-sm btn btn-sm btn-outline-dark" id="reset-search"><i class="la la-refresh mr-1"></i>Clear</button>
						</div> -->
					</div>
				</div>

				<table class="table table-dark table-striped table-hover table-sm" id="table">
					<thead>
						<tr>
							<th>Name</th>
							<th>E-Mail</th>
							<th>Contact</th>
							<th>Gender</th>
							<th>Municipality</th>
							<th>Religion</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody style="display:none;">
						<tr dir-paginate="data in datas | filter : search.filter | filter : {municipality_name : fmunicipality }| itemsPerPage: page_size" current-page="currentPage" pagination-id="pagination">
							<td class="align-middle">{{ data.first_name + ' ' + data.middle_name + ' ' + data.last_name }}</td>
							<td class="align-middle">{{ data.email }}</td>
							<td class="align-middle">{{ data.contact }}</td>
							<td class="align-middle">{{ (data.gender == 'm') ? 'Male' : 'Female'; }}</td>
							<td class="align-middle">{{ data.municipality_name }}</td>
							<td class="align-middle">{{ data.religion }}</td>
							<td>
								<button class="btn btn-outline-light border-0 p-0 btn-sm" data-toggle="modal" data-target="#edit_registrant" ng-click="edit_registrant(data)"><i class="la la-edit la-2x"></i></button>
								<button class="btn btn-outline-light btn-sm border-0 p-0" type="button" ng-click="delete_data(data.id)"><i class="la la-trash-o la-2x"></i></button>
							</td>
						</tr>
					</tbody>
				</table>

				<!-- <div ng-controller="pagination"> -->
					<div class="text-center">
						<dir-pagination-controls boundary-links="true" on-page-change="pages(newPageNumber)" template-url="php/misc/dirpagination.tpl.php" pagination-id="pagination"></dir-pagination-controls>
					</div>
				<!-- </div> -->
			</main>
		</div>
	</div>

	<div class="modal fade" id="edit_registrant" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Edit Record</h5>
					<button type="button" class="close" id="modal-close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					 <form id="edit-registrant-form" class="needs-validation mb-5" novalidate>
						<input type="hidden" name="id" ng-value="selected_registrant.id"/>
						<div class="row">
							<div class="col-md-4 mb-3">
								<label for="first_name">First Name</label>
								<input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" ng-value="selected_registrant.first_name" autofocus required>
								<div class="invalid-feedback">
									Valid first name is required.
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<label for="middle_name">Middle Name</label>
								<input type="text" class="form-control" id="middle_name" name="middle_name" placeholder="Middle Name" ng-value="selected_registrant.middle_name">
							</div>
							<div class="col-md-4 mb-3">
								<label for="last_name">Last Name</label>
								<input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" ng-value="selected_registrant.last_name" required>
								<div class="invalid-feedback">
									Valid last name is required.
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<label for="gender">Gender</label>
								<select class="custom-select d-block w-100" id="gender"  ng-model="selected_registrant.gender" name="gender" required>
									<option value="">Choose...</option>
									<option value="m">Male</option>
									<option value="f">Female</option>
								</select>
								<div class="invalid-feedback">
									Please provide a valid gender.
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<div class="row">
									<div class="col">
										<label for="height">Height</label>
										<input type="text" class="form-control" id="height" name="height" placeholder="Height" ng-value="selected_registrant.height">
									</div>
										<div class="col">
										<label for="weight">Weight</label>
										<input type="text" class="form-control" id="weight" name="weight" placeholder="Weight" ng-value="selected_registrant.weight">
									</div>
								</div>
				            </div>
							<div class="col-md-4 mb-3">
								<label for="birthday">Birthday</label>
								<input type="date" class="form-control" id="birthday" name="birthday" placeholder="" ng-value="selected_registrant.birthday" required>
								<div class="invalid-feedback">
									Valid birthday is required.
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<label for="birthplace">Birth Place</label>
								<input type="text" class="form-control" id="birthplace" name="birthplace" placeholder="Birthplace" ng-value="selected_registrant.birthplace">
							</div>
							<div class="col-md-4 mb-3">
								<label for="civil_status">Civil Status</label>
								<select class="custom-select d-block w-100" id="civil_status" name="civil_status" required>
								</select>
								<div class="invalid-feedback">
									Please provide a valid civil status.
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<label for="religion">Religion</label>
								<input type="text" class="form-control" id="religion" name="religion" placeholder="Religion" ng-value="selected_registrant.religion">
							</div>
							<div class="col-md-4 mb-3">
								<label for="occupation">Occupation</label>
								<input type="text" class="form-control" id="occupation" name="occupation" placeholder="Occupation" ng-value="selected_registrant.occupation">
							</div>
							<div class="col-md-4 mb-3">
				                <label for="spouse_name">Name of Spouse</label>
				                <input type="text" class="form-control" id="spouse_name" name="spouse_name" placeholder="Name of spouse" ng-value="selected_registrant.spouse_name">
				              </div>
				              <div class="col-md-4 mb-3">
				                <label for="num_children">No. of Children</label>
				                <input type="number" class="form-control" id="num_children" name="num_children" placeholder="Number of children" min='0' ng-value="selected_registrant.num_children">
				              </div>
							<div class="col-md-4 mb-3">
								<label for="contact">Contact</label>
								<input type="text" class="form-control" id="contact" name="contact" placeholder="****-***-****" ng-value="selected_registrant.contact">
							</div>
							<div class="col-md-4 mb-3">
								<label for="email">E-Mail</label>
								<input type="text" class="form-control" id="email" name="email" placeholder="you@example.com" ng-value="selected_registrant.email">
								<div class="invalid-feedback">
									Valid email is required.
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<label for="municipality">Municipality</label>
								<select class="custom-select d-block w-100" name="municipality" id="municipality" required></select>
								<div class="invalid-feedback">
									Please provide a valid municipality.
								</div>
							</div>
						</div>

						<div class="mb-3">
							<label for="address">Address</label>
							<textarea type="text" class="form-control" id="address" name="address" ng-value="selected_registrant.address" placeholder="1234 St., XYZ City" style="resize:none;" required></textarea>
							<div class="invalid-feedback">
								Please enter your address.
							</div>
						</div>

						<div class="mb-3">
							<label for="misc">Extra Inormation</label>
							<textarea type="text" class="form-control" id="misc" name="misc" ng-value="selected_registrant.misc" placeholder="Extra information" style="resize:none;"></textarea>
						</div>

						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" id="agreement" name="agreement" required>
							<label class="custom-control-label" for="agreement"><b>Yes, I Agree!</b> As a member of ABAKA, you must support Mayor Sara Duterte's vision which ABAKA adheres to, of a strong Province providing a secure life for all Boholanos, good governance and effective leadership. You must also support the shift towards federalism and PRRD's programs towards the eradication of graft and corruption, drugs and criminality that plague our society today.</label>
						</div>

						<button class="btn btn-primary btn-lg btn-block d-none" type="submit"><i class="la la-save"></i> Save</button>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-primary btn-sm" id="save_record"><i class="la la-save"></i> Save</button>
					<button type="button" class="btn btn-outline-secondary btn-sm" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<a id="download_csv" href="" class="d-none" target="_blank" download />
<?php } require_once($base . '/pages/inc/footer.php'); ?>