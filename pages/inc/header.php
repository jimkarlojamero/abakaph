<?php

	require_once((($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']) . '/php/config/config.php');

?>

<!DOCTYPE html>
<html lang="en" ng-app="ITC" oncontextmenu="return false">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="token" content="<?php echo (!isset($_SESSION['web_app_session_pass_key']))  ? null : $_SESSION['web_app_session_pass_key']; ?>">
	<title><?php echo ($env->app_name) ? $env->app_name : "Info Crowd Tech"; ?></title>

	<script type="text/javascript">
		var base_domain = "<?php echo $base_domain; ?>";
		var is_login = <?php echo WEB_APP_SESSION_STATUS; ?>;
		var global_flag = <?php echo ENV_STATUS; ?>;
	</script>

	<link rel="icon" type="image/png" href="<?php echo $base_domain . '/assets/images/logo.png?v=' . DATE('is'); ?>" onerror="this.href='<?php echo $base_domain ?>/assets/images'" />
    <link href="<?php echo $base_domain ?>/assets/css/bootstrap.min.css?v=<?php echo DATE('is')?>" rel="stylesheet">
    <link href="<?php echo $base_domain ?>/assets/css/all.min.css?v=<?php echo DATE('is')?>" rel="stylesheet">
    <link href="<?php echo $base_domain ?>/assets/css/line-awesome.min.css?v=<?php echo DATE('is')?>" rel="stylesheet">
    <link href="<?php echo $base_domain ?>/assets/css/preloader.css?v=<?php echo DATE('is')?>" rel="stylesheet">
    <link href="<?php echo $base_domain ?>/assets/css/toastr.min.css?v=<?php echo DATE('is')?>" rel="stylesheet">
    <link href="<?php echo $base_domain ?>/assets/css/sweetalert2.min.css?v=<?php echo DATE('is')?>" rel="stylesheet">
    <link href="<?php echo $base_domain ?>/assets/css/animate.css?v=<?php echo DATE('is')?>" rel="stylesheet">
    <link href="<?php echo $base_domain ?>/assets/css/styles.css?v=<?php echo DATE('is')?>" rel="stylesheet">

<?php if (WEB_APP_SESSION_STATUS === 0) { ?>
	<link href="<?php echo $base_domain ?>/assets/css/floating-labels.css?v=<?php echo DATE('is')?>" rel="stylesheet">
<?php } ?>
</head>
<body class="m-0 p-0">
	<div id="preloader" class="animated fadeIn">
		<div id="preview-area" class="minipreloader-wrapper">
			<div class="spinner">
				<div class="dot1"></div>
				<div class="dot2"></div>
			</div>
		</div>
	</div>