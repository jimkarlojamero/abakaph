<?php

	header('Content-type: image/png');

	$text = (!isset($_GET['i'])) ? 'ICT' : substr(strtoupper($_GET['i']), 0, 2);
	$font_face = (($_SERVER['HTTP_HOST'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1)) : $_SERVER['DOCUMENT_ROOT']) . '/assets/fonts/lato-regular-webfont.ttf';
	$font_size = 50;
	$im = imagecreatetruecolor(200, 200);
	$background = ($text === 'ICT') ? imagecolorallocate($im, 21, 31, 63) : imagecolorallocate($im, 236, 240, 241);
	$font_color = ($text === 'ICT') ? imagecolorallocate($im, 255, 255, 255) : imagecolorallocate($im, 127, 140, 141);

	imagefilledrectangle($im, 0, 0, 200, 200, $background);
	list($x, $y) = center_text($im, $text, $font_face, $font_size, 0);
	imagettftext($im, $font_size, 0, $x, $y, $font_color, $font_face, $text);
	imagepng($im);
	imagedestroy($im);


	function center_text($im, $text, $font_face, $font_size, $angle = 0) {
		$xi = imagesx($im);
		$yi = imagesy($im);

		$box = imagettfbbox($font_size, $angle, $font_face, $text);

		$xr = abs(max($box[2], $box[4]));
		$yr = abs(max($box[5], $box[7]));

		$x = intval(($xi - $xr) / 2);
		$y = intval(($yi + $yr) / 2);

		return array($x, $y);
	}

?>