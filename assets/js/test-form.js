var _submit_ctr = 0;
$(function() {
  load_civil_status();
  load_municipality();

  /*document.querySelector('form').addEventListener('submit', (e) => {
    e.preventDefault();
    e.stopPropagation();

    if ($('form').checkValidity() !== false) {
      var data = $('form').serializeArray().reduce(function(obj, item) {
        obj[item.name] = item.value;
        return obj;
      }, {});

      register(data);
    } else {
      $('form').classList.add('was-validated');
    }
  });*/

  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');

    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();

        if (form.checkValidity() !== false) {
          var data = $(form).serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value;
            return obj;
          }, {});

          if (_submit_ctr == 0) {
            show_preloader();
            _submit_ctr = 1;
            register(data);
          }
        }

        form.classList.add('was-validated');
      }, false);
    });
  }, false);
});

function register(_post) {
  
  _post.agreement = (_post.agreement !== 'on') ? 0 : 1;
  // console.log(_post);

  $.ajax({
    url: _uri + 'register',
    type: 'POST',
    data: _post,
    async: true,
    cache: false,
    dataType: 'json',
    crossDomain: false,
    processData: true,
    success: function (data) {
      if (data.response === 'success') {
        toastr["success"](data.message);
        swal({
          title: '',
          text: 'Account successfully registered.', // data.message,
          type: 'success',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Complete'
        }).then((result) => {
          $("input, textarea, select").val('');
          $('input[type="checkbox"]').prop('checked', false);
          $('form').removeClass('was-validated');
        });
      } else {
        toastr["error"](data.message);
        swal('', data.message, 'error');
      }

      _submit_ctr = 0;
      hide_preloader();
    },
    error: function (data) {
      _submit_ctr = 0;
      toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
      hide_preloader();
    }
  });
}

function load_municipality() {
  $.ajax({
    url: _uri + 'municipality',
    type: 'POST',
    // data: _post,
    async: true,
    cache: false,
    dataType: 'json',
    crossDomain: false,
    processData: true,
    success: function (data) {
      _municipaltiy = '<option value="">Choose...</option>';
      data.data.forEach(function(entry) {
        _municipaltiy += '<option value="' + entry.id + '">' + entry.name + '</option>';
      });

      $('select#municipality').html(_municipaltiy);
      hide_preloader();
    },
    error: function (data) {
      toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
      hide_preloader();
    }
  });
}

function load_civil_status() {
  $.ajax({
    url: _uri + 'civil/status',
    type: 'POST',
    // data: _post,
    async: true,
    cache: false,
    dataType: 'json',
    crossDomain: false,
    processData: true,
    success: function (data) {
      civil_status = '<option value="">Choose...</option>';
      data.data.forEach(function(entry) {
        civil_status += '<option value="' + entry.id + '">' + entry.name + '</option>';
      });

      $('select#civil_status').html(civil_status);
      hide_preloader();
    },
    error: function (data) {
      toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
      hide_preloader();
    }
  });
}