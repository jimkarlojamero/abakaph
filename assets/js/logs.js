// Global
var global_folder, global_file_name, global_data, global_process;

toastr.options = {
	"closeButton": false,
	"debug": false,
	"newestOnTop": false,
	"progressBar": false,
	"positionClass": "toast-top-right",
	"preventDuplicates": false,
	"onclick": null,
	"showDuration": "300",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "1000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
};

var ITC = angular.module('ITC', ['angularUtils.directives.dirPagination']);
ITC.controller('controller_logs', controller_logs);
ITC.controller('pagination_logs', pagination_logs);

function controller_logs($scope) {

	$scope.view_logs = function() {
		if (global_data != null) {
			$scope.currentPage = 1;
			$scope.logs_page_size = 10;
			$scope.logs = global_data;
			$scope.$apply();
		}
	}

	$scope.show_data = function(arg1, arg2) {
		$('#pre_content').html('');
		$('#result_modal_title').html('Log Record');

		if (arg1 != null) {
			var jsonViewer = new JSONViewer();
			document.querySelector("#pre_content").appendChild(jsonViewer.getContainer());
			jsonViewer.showJSON(JSON.parse(arg1), arg2);
		}

		$('#result_modal').modal('show');
	}
}

function pagination_logs($scope) {
	$scope.logs_pages = function(num) {
		// // console.log('going to page ' + num);
	};
}

$(window).load(function(){
	if(typeof(EventSource) !== "undefined") {
		var sse = new EventSource(base_domain + "/php/misc/sse.php");
		sse.onmessage = function(event) {
			if (is_login == 1 && event.data != is_login) {
				sse.close();
				window.location.href = "session.php";
			}

			if (is_login == 0 && event.data != is_login) {
				sse.close();
				location.reload();
			}
		};
	}

	temp();
	$("#username").keypress(function (e) { if(e.which == 13) { $("#login_btn").click(); } });
	$("#password").keypress(function (e) { if(e.which == 13) { $("#login_btn").click(); } });
	$("tbody").css("display", "").removeAttr("style");
	hide_preloader();
});

function temp() {
	var _post = {};
	_post.controller = encodeURIComponent('log');
	_post.function_name = encodeURIComponent('date_folder');

	$.ajax({
		url: base_domain + '/tools/log/date_folder',
		type: 'POST',
		data: _post,
		async: true,
		cache: false,
		dataType: 'json',
		crossDomain: false,
		processData: true,
		success: function (data) {
			if (data.response === 'success') {
				$('#date_folder').html(data.data['option']);
			} else {
				toastr["error"](data.message);
			}

			hide_preloader();
		},
		error: function (data) {
			toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
			hide_preloader();
		}
	});
}

$("#date_folder").change(function(){
	var _post = {};
	_post.controller = encodeURIComponent('log');
	_post.function_name = encodeURIComponent('get_folder_data');
	_post.folder_name = encodeURIComponent($("#date_folder").val());

	if (_post.folder_name === 'NULL') {
		$('#log_file').html("<option>No Option(s) Available</option>");
	} else {
		show_preloader();
		$.ajax({
			url: base_domain + '/tools/log/get_folder_data',
			type: 'POST',
			data: _post,
			async: true,
			cache: false,
			dataType: 'json',
			crossDomain: false,
			processData: true,
			success: function (data) {
				if (data.response === 'success') {
					global_folder = _post.folder_name;
					$('#log_file').html(data.data['option']);
				} else {
					toastr["error"](data.message);
				}

				hide_preloader();
			},
			error: function (data) {
				toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
				hide_preloader();
			}
		});
	}
});

$("#log_file").change(function(){
	var _post = {};
	_post.controller = encodeURIComponent('log');
	_post.function_name = encodeURIComponent('get_file_data');
	_post.file_name = encodeURIComponent($("#log_file").val());
	_post.folder_name = encodeURIComponent(global_folder);

	if (_post.log_file === 'NULL') {
		$('#log_file').html("<option>No Option(s) Available</option>");
	} else {
		show_preloader();

		$.ajax({
			url: base_domain + '/tools/log/get_file_data',
			type: 'POST',
			data: _post,
			async: true,
			cache: false,
			dataType: 'json',
			crossDomain: false,
			processData: true,
			success: function (data) {
				global_data = data.data;
				angular.element(document.getElementById('conaiter_logs')).scope().view_logs();
				hide_preloader();
			},
			error: function (data) {
				toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
				hide_preloader();
			}
		});
	}
});

function syntaxHighlight(json, search) {
	json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
	json = json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
		var cls = 'number';
		if (/^"/.test(match)) {
			if (/:$/.test(match)) {
				cls = 'key';
			} else {
				cls = 'string';
			}
		} else if (/true|false/.test(match)) {
			cls = 'boolean';
		} else if (/null/.test(match)) {
			cls = 'null';
		}
		return '<span class="' + cls + '">' + match + '</span>';
	});

	if (search) json = json.replace(new RegExp('('+search+')', 'gi'), '<span class="highlighted">$1</span>');
	
	return json;
}

$("#b_aes_modal").unbind('click').click(function() {
	$('#decrypt_modal_label').html('Decrypt as AES');
	$('#textarea_label').html('Cipher');
	$('#process_request').html('Decrypt');
	$('#encrypted_value').val('');
	global_process = 1;
});

$("#b_jwt_modal").unbind('click').click(function() {
	$('#decrypt_modal_label').html('Decode as JWT');
	$('#textarea_label').html('Bearer');
	$('#process_request').html('Decode');
	$('#encrypted_value').val('');
	global_process = 2;
});

$("#process_request").unbind('click').click(function() {
	var _post = {};
	_post.controller = encodeURIComponent('log');
	_post.function_name = encodeURIComponent(((global_process == 1) ? 'aes_decrypt' : 'jwt_decode'));
	
	if (global_process == 1) {
		_post.aes = encodeURIComponent($('#encrypted_value').val().trim());

		if (!_post.aes || _post.aes == null){
			$('#encrypted_value').focus();
			toastr["error"]("Cipher parameter is empty.");
			return;
		}
	} else {
		_post.jwt = encodeURIComponent($('#encrypted_value').val().trim());

		if (!_post.jwt || _post.jwt == null){
			$('#encrypted_value').focus();
			toastr["error"]("Bearer parameter is empty.");
			return;
		}
	}

	$('#decrypt_modal').modal('hide');
	show_preloader();

	$.ajax({
		url: base_domain + '/tools/log/' + _post.function_name,
		type: 'POST',
		data: _post,
		async: true,
		cache: false,
		dataType: 'json',
		crossDomain: false,
		processData: true,
		success: function (data) {
			if (data.response === 'success') {
				if (global_process == 1) {
					if (!data.data.aes) {
						toastr["error"]("Unable to decrypt cyphered text.");
					} else {
						$('#encrypted_value').val('');
						$('#pre_content').html('');
						$('#result_modal_title').html('Request Result');

						var jsonViewer = new JSONViewer();
						document.querySelector("#pre_content").appendChild(jsonViewer.getContainer());
						jsonViewer.showJSON(data.data);

						$('#result_modal').modal('show');
					}
				} else {
					if (!data.data.jwt) {
						toastr["error"]("Unable to decode bearer text.");
					} else {
						$('#encrypted_value').val('');
						$('#pre_content').html('');
						$('#result_modal_title').html('Request Result');

						var jsonViewer = new JSONViewer();
						document.querySelector("#pre_content").appendChild(jsonViewer.getContainer());
						jsonViewer.showJSON(data.data);

						$('#result_modal').modal('show');
					}
				}
			} else {
				if (global_process == 1) toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
				else toastr["error"](data.message);
			}

			hide_preloader();
		},
		error: function (data) {
			toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
			hide_preloader();
		}
	});
});

$('.modal').on('shown.bs.modal', function() {
  $(this).find('[autofocus]').focus();
});

$("#login_btn").unbind('click').click(function() {
	var _post = {};
	_post.controller = 'env';
	_post.function_name = 'login';
	_post.username = $('#username').val().trim();
	_post.password = $('#password').val().trim();


	if (!_post.username || _post.username == null){
		$('#username').focus();
		toastr["error"]("Username field is empty.");
		return;
	}

	if (!_post.password || _post.password == null){
		$('#password').focus();
		toastr["error"]("Password field is empty.");
		return;
	}

	show_preloader();

	$.ajax({
		url: base_domain + '/tools/env/login',
		type: 'POST',
		data: _post,
		async: true,
		cache: false,
		dataType: 'json',
		crossDomain: false,
		processData: true,
		success: function (data) {
			if (data.response === 'success') {
				window.location.href = "session.php?env_session_id=" + data.data[0].env_session_id + "&env_session_un=" + data.data[0].env_session_un + "&env_session_pass_key=" + data.data[0].env_session_pass_key;
			} else {
				toastr["error"](data.message);
			}

			hide_preloader();
		},
		error: function (data) {
			toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
			hide_preloader();
		}
	});
});


$("#sign_out_btn").unbind('click').click(function() {
	window.location.href = "session.php";
});

function show_preloader() {
	$('#preloader').show().removeClass().addClass('animated fadeIn');
}

function hide_preloader() {
	$('#preloader').removeClass().addClass('fadeOut animated').delay(500).fadeOut(500, function() {
		$(this).removeClass().hide();
	});
}