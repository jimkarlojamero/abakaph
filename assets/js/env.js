toastr.options = {
	"closeButton": false,
	"debug": false,
	"newestOnTop": false,
	"progressBar": false,
	"positionClass": "toast-top-right",
	"preventDuplicates": false,
	"onclick": null,
	"showDuration": "300",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "1000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
};

function randomizer(length) {
	chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

function generate_env(_post) {
	var err_flag = 0;

	$.each(_post, function(key, value) {
		if (typeof $('input[name="' + key + '"]').attr('required') !== 'undefined' && !value) {
			$('input[name="' + key + '"]').focus();
			toastr["error"]($('input[name="' + key + '"]').attr('placeholder'));
			err_flag = 1;
			return false;
		}
	});

	if (err_flag === 1) { return; }

	_post.controller = 'env';
	_post.function_name = 'generate_env';
	show_preloader();

	$.ajax({
		url: base_domain + '/tools/env/generate_env',
		type: 'POST',
		data: _post,
		async: true,
		cache: false,
		dataType: 'json',
		crossDomain: false,
		processData: true,
		success: function (data) {
			if (data.response === 'success') {
				toastr["success"](data.message);
			} else {
				toastr["error"](data.message);
			}

			hide_preloader();
		},
		error: function (data) {
			toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
			hide_preloader();
		}
	});
}

$("#login_btn").unbind('click').click(function() {
	var _post = {};
	_post.controller = 'env';
	_post.function_name = 'login';
	_post.username = $('#username').val().trim();
	_post.password = $('#password').val().trim();


	if (!_post.username || _post.username == null){
		$('#username').focus();
		toastr["error"]("Username field is empty.");
		return;
	}

	if (!_post.password || _post.password == null){
		$('#password').focus();
		toastr["error"]("Password field is empty.");
		return;
	}

	show_preloader();

	$.ajax({
		url: base_domain + '/tools/env/login',
		type: 'POST',
		data: _post,
		async: true,
		cache: false,
		dataType: 'json',
		crossDomain: false,
		processData: true,
		success: function (data) {
			if (data.response === 'success') {
				window.location.href = "session.php?env_session_id=" + data.data[0].env_session_id + "&env_session_un=" + data.data[0].env_session_un + "&env_session_pass_key=" + data.data[0].env_session_pass_key;
			} else {
				toastr["error"](data.message);
			}

			hide_preloader();
		},
		error: function (data) {
			toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
			hide_preloader();
		}
	});
});

$('#s_push').unbind('click').click(function() {
	var _post = {};
	_post.controller = 'env';
	_post.function_name = 'send_push';
	/*_post.app_name = $('#val_app_name').val().trim();
	_post.fcm = $('#val_fcm').val().trim();*/
	_post.push_id = $('#push_id').val().trim();
	_post.message = $('#push_msg').val().trim();

	/*if (!_post.app_name || _post.app_name == null){
		$('#app_name').focus();
		toastr["error"]("Please set propper value on APP Name field.");
		return;
	}

	if (!_post.fcm || _post.fcm == null){
		$('#fcm').focus();
		toastr["error"]("Please set propper value on FCM field.");
		return;
	}*/

	if (!_post.push_id || _post.push_id == null){
		$('#push_id').focus();
		toastr["error"]("Please set propper value on Push ID field.");
		return;
	}

	if (!_post.message || _post.message == null){
		$('#push_msg').focus();
		toastr["error"]("Please set propper value on Message field.");
		return;
	}

	show_preloader();

	$.ajax({
		url: base_domain + '/tools/env/send_push',
		type: 'POST',
		data: _post,
		async: true,
		cache: false,
		dataType: 'json',
		crossDomain: false,
		processData: true,
		success: function (data) {
			// if (data.response === 'success') {
			// 	toastr["success"](data.message);
			// } else {
			// 	toastr["error"](data.message);
			// }

			hide_preloader();
		},
		error: function (data) {
			toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
			hide_preloader();
		}
	});
});

$("#sign_out_btn").unbind('click').click(function() {
	window.location.href = "session.php";
});

function show_preloader() {
	$('#preloader').show().removeClass().addClass('animated fadeIn');
}

function hide_preloader() {
	$('#preloader').removeClass().addClass('fadeOut animated').delay(500).fadeOut(500, function() {
		$(this).removeClass().hide();
	});
}

$(function() {
	$('#environment').on('submit', function(e) {
		e.preventDefault();

		var data = $('#environment').serializeArray().reduce(function(obj, item) {
			obj[item.name] = item.value;
			return obj;
		}, {});

		generate_env(data);
	});

	$('button[data-target]').on('click', function(e) {
		e.preventDefault();
		var element = e.currentTarget.attributes['data-target'].value;
		var plugin = $('[name="' + element + '"]').data('plugin');
		var length = (typeof $('[name="' + element + '"]').data(plugin) !== 'undefinded') ? $('[name="' + element + '"]').data(plugin) : null;

		if (plugin === 'randomizer') {
			$('[name="' + element + '"]').val(window[plugin](length));
		}
	});

	$("#username").keypress(function (e) { if(e.which == 13) { $("#login_btn").click(); } });
	$("#password").keypress(function (e) { if(e.which == 13) { $("#login_btn").click(); } });
	hide_preloader();
})