// Global
var global_data, global_process, timeout, page_size_no, page_no;
var ITC = angular.module('ITC', ['angularUtils.directives.dirPagination']);
	ITC.controller('controller', controller);
// ITC.controller('pagination', pagination);

function controller($scope) {
	$scope.municipality = [];
	$scope.total_count = 0;
	//$scope.pageQuery = "registrants";

	$scope.view_data = function() {
		if (global_data != null) {
			//$scope.currentPage = 1;
			$scope.page_size = page_size_no == typeof "undefined" ? 20 : page_size_no;
			$scope.datas = global_data;
			$scope.$apply();
			$('tbody').show();
		}
	}

	$scope.pages = function(num) {
		
		page_no = num;
		if ($scope.pageQuery == "registrants") {
			get_registrants(num);
		} else if ($scope.pageQuery == "municipality") {
			municipality(num);
		} else if ($scope.pageQuery == "search_val") {
			searchfilter(num);
		} else if ($scope.pageQuery == "pagination") {
			item_per_page(num);
		}
	};

	// Filter By name query...
	$scope.show_data = function(arg1, arg2) {
		$('#pre_content').html('');
		$('#result_modal_title').html('Log Record');

		if (arg1 != null) {
			var jsonViewer = new JSONViewer();
			document.querySelector("#pre_content").appendChild(jsonViewer.getContainer());
			jsonViewer.showJSON(JSON.parse(arg1), arg2);
		}

		$('#result_modal').modal('show');
	}

	$scope.delete_data = function (data) {
		Swal({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.value) {
				var registrant_info = {};
				registrant_info.id = data;
				
				show_preloader();

				$.ajax({
					url: _uri + 'fetch/delete_registrant',
					type: 'POST',
					data: registrant_info,
					async: true,
					cache: false,
					dataType: 'json',
					crossDomain: false,
					processData: true,
					success: function (data) {
						swal({
				          title: '',
				          text: 'Account has been removed.', // data.message,
				          type: 'success',
				          confirmButtonColor: '#3085d6',
				          confirmButtonText: 'Complete'
				        }).then((result) => {
				        	get_registrants(1);
				        });

				        $scope.$apply();
						hide_preloader();
					},
					error: function (data) {
						toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
						hide_preloader();
					}
				});
			}
		});
	}

	$scope.edit_registrant = function (data) {
		$('#edit_registrant').removeClass('was-validated');
		$.each(data, function(k, v) {
			if (typeof $('[name="' + k + '"]') !== 'undefined' && k != 'agreement') {
				$('[name="' + k + '"]').val(v);
			}
		});

		if (data.agreement == 1) {
			$('#agreement').prop('checked', true);
		}

		$('#municipality').val(data.municipality);
		$('#civil_status').val(data.civil_status);
	}

	/*$scope.save_edit = function (data) {
		console.log(data);
	}*/

}

// function pagination($scope) {
// 	$scope.pages = function(num) {
// 		console.log('going to page ' + num);
// 	};
// }

function get_registrants(data) {
	var page_val = {};
		page_val.pagenum = data;
		page_val.itemperpage = 20;

	show_preloader();
	$.ajax({
		url: _uri + 'fetch/registrants',
		type: 'POST',
		data: page_val,
		async: true,
		cache: false,
		dataType: 'json',
		crossDomain: false,
		processData: true,
		success: function (data) {
			if (data.response == "success") {
				global_data = data.data.data;
				page_size_no = 20;
				angular.element(document.getElementById('container')).scope().currentPage = page_val.pagenum;
				angular.element(document.getElementById('container')).scope().total_count = data.data.total_count;
				angular.element(document.getElementById('container')).scope().view_data();
				angular.element(document.getElementById('container')).scope().pageQuery = "registrants";
			}
			hide_preloader();
		},
		error: function (data) {
			toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
			hide_preloader();
		}
	});
}

$("#municipality").on("change", function() {
	municipality(1);
}); 

$('#search').on('keyup', function(){
	searchfilter(1);
});

$("#items_per_page").on('keyup input', function() {
	item_per_page(1);
});

$("#export").on("click", function() {
	export_csv();
});

function export_csv () {

	var filter = {};
		filter.pageno = page_no == typeof "undefined" ? 1 : page_no;
		filter.itemperpage = $("#items_per_page").val();
		filter.municipality = $("#municipality").val();
		filter.searches = $("#search").val();
		console.log(filter);

	Swal({
		title: 'Are you sure?',
		text: "Export to csv file!",
		type: 'question',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, Export it!'
	}).then((result) => {
		if (result.value) {
			 $.ajax({
				url: _uri + 'abaka/generate',
				type: 'POST',
				//data: municipality,
				async: true,
				cache: false,
				dataType: 'json',
				crossDomain: false,
				processData: true,
				success: function (data) {
					if (data.response == 'success') {
						swal({
							title: '',
							text: 'File successfully Exported.', // data.message,
							type: 'success',
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'Complete'
						}).then((result) => {
							//get_registrants(1);
							$('#modal-close').click();
							hide_preloader();
						});
					} else {
						toastr["error"](data.message);
					}
					hide_preloader();
				},
				error: function (data) {
					toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
					hide_preloader();
				}
			});
		}
	});
}

function item_per_page (_data) {

	var paginate = {};
		paginate.pagenum = _data;
		paginate.itemperpage = $("#items_per_page").val() == "" ? "20" : $("#items_per_page").val();
		paginate.search_val = $("#search").val();
		paginate.municipality = $("#municipality").val();

		console.log(paginate);

		$.ajax({
			url: _uri + 'fetch/itemsperpage',
			type: 'POST',
			data: paginate,
			async: true,
			cache: false,
			dataType: 'json',
			crossDomain: false,
			processData: true,
			success: function (data) {
				if (data.response == 'success') {

					global_data = data.data.data;
					page_size_no = data.data.item_size;
					angular.element(document.getElementById('container')).scope().currentPage = data.data.pageno;
					angular.element(document.getElementById('container')).scope().pageQuery = "pagination";
					angular.element(document.getElementById('container')).scope().total_count = data.data.total_count;

				} else {
					global_data = [];
				}

				//angular.element(document.getElementById('container')).scope().$apply();
		        angular.element(document.getElementById('container')).scope().view_data();
				hide_preloader();
			},
			error: function (data) {
				toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
				hide_preloader();
			}
		});

}

function municipality (limit) {

	if ($("#municipality").val() == "") {
		get_registrants(1);
		return;
	}

	var municipality = {};
		municipality.pagenum = limit;
		municipality.pageitems = 20;
		municipality.search_val = $('#search').val();
		municipality.query_value = $('#municipality').val();
		
		//console.log(municipality);
		show_preloader();
		
		$.ajax({
			url: _uri + 'fetch/municipality_filter',
			type: 'POST',
			data: municipality,
			async: true,
			cache: false,
			dataType: 'json',
			crossDomain: false,
			processData: true,
			success: function (data) {
				if (data.response == 'success') {

					global_data = data.data.data;
					page_size_no = 20;
					angular.element(document.getElementById('container')).scope().currentPage = limit;
					angular.element(document.getElementById('container')).scope().pageQuery = "municipality";
					angular.element(document.getElementById('container')).scope().total_count = data.data.total_count;

				} else {
					global_data = [];
				}

				//angular.element(document.getElementById('container')).scope().$apply();
		        angular.element(document.getElementById('container')).scope().view_data();
				hide_preloader();
			},
			error: function (data) {
				toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
				hide_preloader();
			}
		});
	}

	function searchfilter (limit) {

		var search = {};
			search.search_query = $('#search').val();
			search.query_val = $("#municipality").val();
			search.pagenum = limit;
			search.pageitems = 20;

			if($("#search").val() == "") {
				get_registrants(1);
				return;
			}
			
		clearTimeout(timeout);

		timeout = setTimeout(function () {
        	show_preloader();

        	$.ajax({
				url: _uri + 'fetch/search_query',
				type: 'POST',
				data: search,
				async: true,
				cache: false,
				dataType: 'json',
				crossDomain: false,
				processData: true,
				success: function (data) {
					if (data.response == 'success') {
						global_data = data.data.data;
						page_size_no = 20;
						angular.element(document.getElementById('container')).scope().currentPage = search.pagenum;
						angular.element(document.getElementById('container')).scope().pageQuery = "search_val";
						angular.element(document.getElementById('container')).scope().total_count = data.data.total_count;
					} else {
						global_data = [];
					}

			        //angular.element(document.getElementById('container')).scope().$apply();
			        angular.element(document.getElementById('container')).scope().view_data();
					hide_preloader();
				},
				error: function (data) {
					toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
					hide_preloader();
				}
			});

    	}, 500);
	}

$(function() {
	if (is_login !== 0) {
		load_municipality();
		load_civil_status();
		get_registrants(1);

		// $('#reset-search').on('click', function() {
		// 	angular.element(document.getElementById('container')).scope().search = '';
		// 	angular.element(document.getElementById('container')).scope().search2 = '';
		// 	//angular.element(document.getElementById('container')).scope().$apply();
		// 	$('.municipality').val('');
		// });

		$('#save_record').on('click', function() {
			$("form button[type='submit']").click();
		});

		if (document.querySelector('#edit-registrant-form') !== null) {
			document.querySelector('#edit-registrant-form').addEventListener('submit', (e) => {
			    e.preventDefault();
			    e.stopPropagation();

			    if ($('#agreement').is(':checked')) {
					var data = $('#edit-registrant-form').serializeArray().reduce(function(obj, item) {
						obj[item.name] = item.value;
						return obj;
					}, {});

					edit_registrant(data);
			    } else {
					$('#edit_registrant').addClass('was-validated');
			    }
			});
		}
	}
});

function edit_registrant(data) {
  	Swal({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, update it!'
	}).then((result) => {
		if (result.value) {
		  	var edit_registrant = data;
			edit_registrant.agreement = (edit_registrant.agreement !== 'on') ? 0 : 1;

			$.ajax({
			    url: _uri + 'update/members',
			    type: 'POST',
			    data: edit_registrant,
			    async: true,
			    cache: false,
			    dataType: 'json',
			    crossDomain: false,
			    processData: true,
			    success: function (data) {
					if (data.response === 'success') {
						toastr["success"](data.message);
						swal({
							title: '',
							text: 'Account successfully Updated.', // data.message,
							type: 'success',
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'Complete'
						}).then((result) => {
							get_registrants(1);
							$('#modal-close').click();
							$('form').trigger("reset");
							$('form').removeClass('was-validated');
							hide_preloader();
						});
					} else {
						toastr["error"](data.message);
						swal('', data.message, 'error');
					}
			    },
			    error: function (data) {
					toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
					hide_preloader();
			    }
			});
		}
	});
}

function load_municipality() {
 
  $.ajax({
    url: _uri + 'municipality',
    type: 'POST',
    // data: _post,
    async: true,
    cache: false,
    dataType: 'json',
    crossDomain: false,
    processData: true,
    success: function (data) {
      _municipality = '<option value="">Choose...</option>';
      _municipality_2 = '<option value="">Choose...</option>';

      data.data.forEach(function(entry) {
        _municipality += '<option value="' + entry.id + '" data-id="' + entry.id + '">' + entry.name + '</option>';
        _municipality_2 += '<option value="' + entry.name + '" data-id="' + entry.id + '">' + entry.name + '</option>';
      });

      $('select#municipality').html(_municipality);
      $('select.municipality').html(_municipality_2);
      hide_preloader();
    },
    error: function (data) {
      toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
      hide_preloader();
    }
  });
}

function load_civil_status() {
  $.ajax({
    url: _uri + 'civil/status',
    type: 'POST',
    // data: _post,
    async: true,
    cache: false,
    dataType: 'json',
    crossDomain: false,
    processData: true,
    success: function (data) {
      civil_status = '<option value="">Choose...</option>';
      data.data.forEach(function(entry) {
        civil_status += '<option value="' + entry.id + '">' + entry.name + '</option>';
      });

      $('select#civil_status').html(civil_status);
      hide_preloader();
    },
    error: function (data) {
      toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
      hide_preloader();
    }
  });
}