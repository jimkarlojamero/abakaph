// Global
var global_data, global_process, timeout, page_size_no, page_no;
var ITC = angular.module('ITC', ['angularUtils.directives.dirPagination']);
	ITC.controller('front-controller', frontcontroller);

$(function() {
	if (is_login !== 0) {
		load_municipality();
		load_civil_status();
		fget_registrants();

		$('#reset-search').on('click', function() {
			angular.element(document.getElementById('container')).scope().search = '';
			angular.element(document.getElementById('container')).scope().search2 = '';
			//angular.element(document.getElementById('container')).scope().$apply();
			$('.municipality').val('');
		});

		$('#save_record').on('click', function() {
			$("form button[type='submit']").click();
		});

		if (document.querySelector('#edit-registrant-form') !== null) {
			document.querySelector('#edit-registrant-form').addEventListener('submit', (e) => {
			    e.preventDefault();
			    e.stopPropagation();

			    if ($('#agreement').is(':checked')) {
					var data = $('#edit-registrant-form').serializeArray().reduce(function(obj, item) {
						obj[item.name] = item.value;
						return obj;
					}, {});

					edit_registrant(data);
			    } else {
					$('#edit_registrant').addClass('was-validated');
			    }
			});
		}
	}
});

function edit_registrant(data) {
  	Swal({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, update it!'
	}).then((result) => {
		if (result.value) {
		  	var edit_registrant = data;
			edit_registrant.agreement = (edit_registrant.agreement !== 'on') ? 0 : 1;

			$.ajax({
			    url: _uri + 'update/members',
			    type: 'POST',
			    data: edit_registrant,
			    async: true,
			    cache: false,
			    dataType: 'json',
			    crossDomain: false,
			    processData: true,
			    success: function (data) {
					if (data.response === 'success') {
						toastr["success"](data.message);
						swal({
							title: '',
							text: 'Account successfully Updated.', // data.message,
							type: 'success',
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'Complete'
						}).then((result) => {
							fget_registrants();
							$('#modal-close').click();
							$('form').trigger("reset");
							$('form').removeClass('was-validated');
							hide_preloader();
						});
					} else {
						toastr["error"](data.message);
						swal('', data.message, 'error');
					}
					
			    },
			    error: function (data) {
					toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
					hide_preloader();
			    }
			});
		}
	});
}

function frontcontroller($scope) {

	$scope.view_data = function() {
		if (global_data != null) {
			$scope.currentPage = 1;
			$scope.page_size = 20;
			$scope.datas = global_data;
			$scope.$apply();
			$('tbody').show();
		}
	}

	$scope.delete_data = function (data) {
		Swal({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.value) {
				var registrant_info = {};
				registrant_info.id = data;
				
				show_preloader();

				$.ajax({
					url: _uri + 'fetch/delete_registrant',
					type: 'POST',
					data: registrant_info,
					async: true,
					cache: false,
					dataType: 'json',
					crossDomain: false,
					processData: true,
					success: function (data) {
						swal({
				          title: '',
				          text: 'Account has been removed.', // data.message,
				          type: 'success',
				          confirmButtonColor: '#3085d6',
				          confirmButtonText: 'Complete'
				        }).then((result) => {
				        	fget_registrants();
				        });

				        $scope.$apply();
						hide_preloader();
					},
					error: function (data) {
						toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
						hide_preloader();
					}
				});
			}
		});
	}


	$scope.edit_registrant = function (data) {
		$('#edit_registrant').removeClass('was-validated');
		$.each(data, function(k, v) {
			if (typeof $('[name="' + k + '"]') !== 'undefined' && k != 'agreement') {
				$('[name="' + k + '"]').val(v);
			}
		});

		if (data.agreement == 1) {
			$('#agreement').prop('checked', true);
		}

		$('#municipality').val(data.municipality);
		$('#civil_status').val(data.civil_status);
	}

}

function fget_registrants () {

	show_preloader();

	$.ajax({
		url: _uri + 'front/fget_registrants',
		type: 'POST',
		//data: page_val,
		async: true,
		cache: false,
		dataType: 'json',
		crossDomain: false,
		processData: true,
		success: function (data) {
			if (data.response == "success") {
				global_data = data.data;
				angular.element(document.getElementById('container')).scope().view_data();
			}

			hide_preloader();
		},
		error: function (data) {
			toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
			hide_preloader();
		}
	});
}

$("#fitems_per_page").on("keyup input", function (e) {

	if (e.target.value == "") {
		angular.element(document.getElementById('container')).scope().page_size = 20;
		angular.element(document.getElementById('container')).scope().$apply();
	} else {
		angular.element(document.getElementById('container')).scope().page_size = e.target.value;
		angular.element(document.getElementById('container')).scope().$apply();
	}

});



$("#export").on("click", function() {
	export_csv();
});

function export_csv () {
	var filter = {};
		filter.pageno = page_no == typeof "undefined" ? 1 : page_no;
		filter.itemperpage = $("#items_per_page").val();
		filter.municipality = $("#fmunicipality").val();
		filter.search = $("#fsearch").val();
		filter.filter = false;
		// console.log(filter);

	Swal({
		title: 'Do you want?',
		text: "To export the data with the search filters!",
		type: 'question',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#2ecc71',
		confirmButtonText: 'Yes, Include it!',
		cancelButtonText: 'No, Just proceed!',
		allowOutsideClick: false
	}).then((result) => {
		show_preloader();
		if (result.value) {
			filter.filter = true;
		}

		$.ajax({
			url: _uri + 'abaka/generate',
			type: 'POST',
			data: filter,
			async: true,
			cache: false,
			dataType: 'json',
			crossDomain: false,
			processData: true,
			success: function (data) {
				if (data.response == 'success') {
					swal({
						title: '',
						text: 'File successfully Exported.', // data.message,
						type: 'success',
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'Complete',
						allowOutsideClick: false
					}).then((result) => {
				        console.log(data);
						//get_registrants(1);
						// document.getElementById('download_csv').src = data.data.filename;
						// $('#download_csv').attr('href', _uri + 'assets/dump/' + data.data.filename);
						// $('#download_csv').click();
						var link = document.createElement("a");
							link.download = data.data.filename;
							link.href = _uri + 'assets/dump/' + data.data.filename;
							link.click();
						$('#modal-close').click();
						hide_preloader();
					});
				} else {
					toastr["error"](data.message);
				}
				hide_preloader();
			},
			error: function (data) {
				toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
				hide_preloader();
			}
		});
	});
}

function load_municipality() {
 
  $.ajax({
    url: _uri + 'municipality',
    type: 'POST',
    // data: _post,
    async: true,
    cache: false,
    dataType: 'json',
    crossDomain: false,
    processData: true,
    success: function (data) {
      _municipality = '<option value="">Choose...</option>';
      _municipality_2 = '<option value="">Choose...</option>';

      data.data.forEach(function(entry) {
        _municipality += '<option value="' + entry.id + '" data-id="' + entry.id + '">' + entry.name + '</option>';
        _municipality_2 += '<option value="' + entry.name + '" data-id="' + entry.id + '">' + entry.name + '</option>';
      });

      $('select#municipality').html(_municipality);
      $('select.municipality').html(_municipality_2);
      hide_preloader();
    },
    error: function (data) {
      toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
      hide_preloader();
    }
  });
}

function load_civil_status() {

  $.ajax({
    url: _uri + 'civil/status',
    type: 'POST',
    // data: _post,
    async: true,
    cache: false,
    dataType: 'json',
    crossDomain: false,
    processData: true,
    success: function (data) {
      civil_status = '<option value="">Choose...</option>';
      data.data.forEach(function(entry) {
        civil_status += '<option value="' + entry.id + '">' + entry.name + '</option>';
      });

      $('select#civil_status').html(civil_status);
      hide_preloader();
    },
    error: function (data) {
      toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
      hide_preloader();
    }
  });
}

