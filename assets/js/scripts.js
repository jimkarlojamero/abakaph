var _uri = window.location.protocol + '//' + window.location.host + window.location.pathname.replace(window.location.pathname.substring(window.location.pathname.lastIndexOf('/')+1), '');

toastr.options = {
	"closeButton": false,
	"debug": false,
	"newestOnTop": false,
	"progressBar": false,
	"positionClass": "toast-top-right",
	"preventDuplicates": false,
	"onclick": null,
	"showDuration": "300",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "1000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
};

$(function() {

	if (typeof is_login !== 'undefined' && is_login === 0) {
		document.querySelector('#login').addEventListener('submit', (e) => {
			e.preventDefault();

			var data = $('#login').serializeArray().reduce(function(obj, item) {
				obj[item.name] = item.value;
				return obj;
			}, {});

			login(data);
		});
	}

	if (typeof $('#execute-signup').val() !== 'undefined') {
		document.querySelector('#execute-signup').addEventListener('click', (e) => {
			window.location.href = _uri + 'signup';
		});
	}

	if (typeof $('#sign_out_btn').val() !== 'undefined') {
		document.querySelector('#sign_out_btn').addEventListener('click', (e) => {
			window.location.href = _uri + 'php/misc/session.php';
		});
	}

	hide_preloader();

});

function show_preloader() {
	$('#preloader').show().removeClass().addClass('animated fadeIn');
}

function hide_preloader() {
	$('#preloader').removeClass().addClass('fadeOut animated').delay(500).fadeOut(500, function() {
		$(this).removeClass().hide();
	});
}

function login(_post) {
	var landing_page = "front-registrants";
	show_preloader();

	if(!_post.username) {
		toastr["warning"]('Username field is required!');
		swal('', 'Username field is required!', 'warning');
		hide_preloader();
		return;
	}

	if (!_post.password) {
		toastr["warning"]('Password field is required!');
		swal('', 'Password field is required!', 'warning');
		hide_preloader();
		return;
	}

	$.ajax({
		url: _uri + 'abaka/login',
		type: 'POST',
		data: _post,
		async: true,
		cache: false,
		dataType: 'json',
		crossDomain: false,
		processData: true,
		success: function (data) {
			if (data.response === 'success') {
				window.location.href = "php/misc/session.php?web_app_session_id=" + data.data[0].web_app_session_id + "&web_app_session_un=" + data.data[0].web_app_session_un + "&web_app_session_pass_key=" + data.data[0].web_app_session_pass_key + "&destination=" + landing_page
			} else {
				toastr["error"](data.message);
				swal('', data.message, 'error');
			}

			hide_preloader();
		},
		error: function (data) {
			toastr["error"]("There is an error with the server response. Please coordinate with your System's Administrator.");
			hide_preloader();
		}
	});

}